//============================================================================
// Name        : test_VecAdd.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <vector>
#include <iostream>
#include <spinus.h>
#include <unistd.h>
#include <time.h>

#include <stdio.h>

#include <cilk/cilk.h>
//#include <complex.h>

struct cc
{
	double r;
	double i;
};

using namespace std;

long int getTick() {
    struct timespec ts;
    double time;
    clock_gettime( CLOCK_REALTIME, &ts );
    time  = (double)ts.tv_nsec/1000. ;
    time += ts.tv_sec * 1000000;
    return time;
}

void test_add_vec()
{

	cout<<"------ test  v3=a*v1+v2 ------"<<endl;
//	mkl_set_dynamic(0);
//	omp_set_nested(0);

	int n=1000000;
	vector<MKL_Complex16> v1(n,{0,1}), v2(n,2), v3(n,0);

	MKL_Complex16 val=3.;


	cout<<"own"<<endl;
	for(int i=0;i<10;i++)
	{
		long int t = getTick();
		memcpy(v3.data(),v2.data(),sizeof(MKL_Complex16)*n);
		cblas_zaxpy (n, &val, v1.data(), 1, v3.data(), 1);
		cout<<"MKL time: "<< getTick()-t<<" testval:"<< v3[0]<<endl;

		t = getTick();
//		sps::sps_zaxpy(n,val,v1.data(),v2.data(),v3.data());
#pragma omp parallel for simd vectorlength(2)
		for(int j=0;j<n;j++)
		{
			v3[j]=val*v1[j]+v2[j];
		}
		cout<<"OWN time: "<< getTick()-t<<" testval:"<< v3[0]<<endl;
	}

}



void test_mat_vec()
{
	cout<<"------ test  v2=a*H*v1+b*v1 ------"<<endl;

//	mkl_set_num_threads(1);
	mkl_set_dynamic(0);
	omp_set_nested(0);
//	omp_set_num_threads(1);

	int n=4000;
	vector<MKL_Complex16> v1(n,1.0), v2(n,0.0), H(n*n,1.0), tmp(n,0.), v3(n,1.0);

	MKL_Complex16 a(1,2), b(2,1);

	MKL_Complex16* v1data= v1.data();
	MKL_Complex16* mdat = H.data();
	MKL_Complex16* tmpdat = tmp.data();


	for(int k=0;k<5;k++)
	{
		/////////////////////////////////////////////////////////////////////////////
		///// MKL ////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
		long int t = getTick();


		memcpy(v2.data(),v1.data(),sizeof(MKL_Complex16)*n);
		cblas_zhemv(CBLAS_ORDER::CblasRowMajor, CBLAS_UPLO::CblasLower, n, &a, H.data(), n, v1.data(), 1, &b, v2.data(), 1);


		cout<<"MKL time: "<< getTick()-t<<endl;

		////////////////////////////////////////////////////////////////////
		//////// My 1 /////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
		t = getTick();

#pragma omp parallel for
		for(int i=0;i<n;i++)
		{
			MKL_Complex16 *subv = &(mdat[i*n]);

			MKL_Complex16 val= __sec_reduce(complex<double>(0,0), subv[0:n] * v1data[0:n], operator+ );

			v2[i]= a*val+b*v1[i];
		}
		//cilk_sync;
		cout<<"OWN time 1st variant: "<< getTick()-t<<"; testval "<<v2[0]<< endl;


		////////////////////////////////////////////////////////////////////
		//////// My 2 /////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
		t = getTick();

#pragma omp parallel for
		for(int i=0;i<n;i++)
		{
			MKL_Complex16 *subv = &(mdat[i*n]);

			double valr = 0.0, vali=0.0;


#pragma simd  vectorlengthfor(MKL_Complex16) reduction(+:valr,vali) //shared(val)
			for(int j=0;j<n;j++)
			{
				MKL_Complex16 val = subv[j] * v1data[j];
				//s +=  (subv[j] * v1data[j]).__rep();
				valr +=  val.real();
				vali +=  val.imag();
			}

			//v2[i]= a*MKL_Complex16(valr,vali)+b*v1[i];
			v2[i]= a*MKL_Complex16(valr,vali)+b*v1[i];

		}

		cout<<"OWN time 1st variant: "<< getTick()-t<<"; testval "<<v2[0]<< endl;



	}




}


void test_dot()
{
	cout<<"------ test  v2=a*H*v1+b*v1 ------"<<endl;

	mkl_set_num_threads(1);
	mkl_set_dynamic(0);
	omp_set_nested(1);
	omp_set_num_threads(1);

	int n=2000;
	vector<MKL_Complex16> v1(n,1.0), v2(n,0.0), v3(n,1.0);

	MKL_Complex16 a(1,2), b(2,1);

	MKL_Complex16* v1data= v1.data();
	MKL_Complex16* v2data= v2.data();


	for(int i=0;i<n;i++)
	{

	}

}

int main() {
	cout << "Hello World!!!" << endl; // prints Hello World!!!



	test_add_vec();

	test_mat_vec();

	test_dot();

	return 0;
}
