/*
 * main.cpp
 *
 *  Created on: Jan 11, 2016
 *      Author: isivkov
 */


#include <iostream>
#include <vector>

using namespace std;

long double getTick() {
    struct timespec ts;
    double time;
    clock_gettime( CLOCK_REALTIME, &ts );
    time  = (double)ts.tv_nsec/1000. ;
    time += ts.tv_sec * 1000000;
    return time;
}

int main()
{

	int n=3;

	double f=1;


	long int t=getTick();

#pragma simd reduction(*:f)
	for(int i=1;i<n;i++)
	{
		f*=i;
	}
	cout<<"time: "<< (getTick()-t)/1000 <<endl;
	cout<<"val "<<f<<endl;

	cout<<(1<<4)<<endl;
	return 0;
}
