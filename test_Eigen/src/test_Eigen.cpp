//============================================================================
// Name        : test_Eigen.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================



#include <iostream>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <spinus.h>
#include <unistd.h>
#include <time.h>


using namespace Eigen;
using namespace std;
using namespace sps;


typedef Eigen::SparseMatrix<double,Eigen::Lower> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::SparseMatrix<int> SpMatI;
typedef Eigen::SparseMatrix<MKL_Complex16, Eigen::Lower> SpMatC16;
typedef Eigen::Triplet<double> T;


long int getTick() {
    struct timespec ts;
    double time;
    clock_gettime( CLOCK_REALTIME, &ts );
    time  = (double)ts.tv_nsec/1000. ;
    time += ts.tv_sec * 1000000;
    return time;
}

void test_base();
void test_big_matr();
void test_triplet();
void test_trsparse();
void test_rembo();
void test_sum();
void test_map();

int main() {

//	Eigen::initParallel();

//	test_base();

//	test_big_matr();

//	test_triplet();

//	test_trsparse();

//	test_rembo();

//	test_sum();


	test_map();



	cout << "Hello World!!!" << endl; // prints Hello World!!!
	return 0;
}

void test_base()
{
	cout<<"--------Test base-----------------"<<endl;

	MatrixXd m = MatrixXd::Random(3,3);
		m = (m + MatrixXd::Constant(3,3,1.2)) * 50;
		cout << "m =" << endl << m << endl;
		VectorXd v(3);
		v << 1, 2, 3;
		cout << "m * v =" << endl << m * v << endl;


		vector<double> adns1={1,0,3,
							0,0,4,
							3,4,0};

		vector<double> adns2={0,6,0,
							0,5,4,
							0,0,0};

		SpMat sm1,sm2;

		sm1.resize(3,3);      //Change sm1 to a m x n matrix.
		sm1.reserve(3);     // Allocate room for nnz

		sm2.resize(3,3);
		sm2.reserve(3);

		for(int i=0;i<3;i++)
		{
			for(int j=0; j<3; j++)
			{
				if(adns1[i*3+j]!=0)
				{
					sm1.insert(i,j)=adns1[i*3+j];
				}

				if(adns2[i*3+j]!=0)
				{
					sm2.insert(i,j)=adns2[i*3+j];
				}
			}
		}

		//sm1.makeCompressed();
		//sm2.makeCompressed();

		SpMat sm3 = sm1+sm2;

		cout<< sm1 <<endl;
		cout<< sm2 <<endl;
		cout<< sm3 <<endl;

		//sm3.makeCompressed();

		sm1.makeCompressed();
		double *val3 = sm1.valuePtr();

		int nz = sm1.nonZeros();

		for(int i=0;i<nz;i++)
		{
			cout<< val3[i]<<" ";
		}
		cout<<endl;

		cout<< "size: "<<sm3.size()<<endl;

		cout<<"-------- Done! -----------------"<<endl;
}



void test_big_matr()
{
	cout<<"--------Test Big matrices -----------------"<<endl;

	cout<<"create spin states"<<endl;

	JJState jjs(3);
	jjs.PlusJ(3);
	jjs.PlusJ(3);
	jjs.PlusJ(3);
	jjs.PlusJ(3);
	jjs.PlusJ(3);
	jjs.PlusJ(3);


	MatrixBuilder mb;

	int n = jjs.GetNumParticles();

	SpinQOp op(0,n);
	SpinQOp op2(0,n);

	vector<Tensor*> c0 = op.GetComponents(0);
	vector<Tensor*> c1 = op2.GetComponents(2);

	mb.SetJJState(&jjs);

	cout<<"Build basis"<<endl;
	jjs.BuildBasis();
	int dim = jjs.GetBasisDimension();

	cout<<"allocate dense matrices"<<endl;

	MatrixC *mat0 = new MatrixC(dim,dim,0);
	MatrixC *mat1 = new MatrixC(dim,dim,0);

	cout<<"create spin matrices"<<endl;

	mb.BuildFromTensor(c0, mat0);
	mb.BuildFromTensor(c1, mat1);

	MKL_Complex16 *dat0 = mat0->GetData();
	MKL_Complex16 *dat1 = mat1->GetData();

	cout<<"init sparse matr"<<endl;
	//sparse
	SpMatC16 sm0, sm1;

	sm0.resize(dim,dim);
	sm1.resize(dim,dim);

//	sm0.reserve(VectorXi::Constant(dim,100));
//	sm1.reserve(VectorXi::Constant(dim,100));

	typedef Eigen::Triplet<MKL_Complex16> T;
	std::vector<T> tl0, tl1;
	tl0.reserve(dim*100);
	tl1.reserve(dim*100);

	cout<<"fill sparse matrices with triplets"<<endl;
	//fill up the sparse matrices

	////////////////////////////////////////////////////
	long int t=getTick();
	////////////////////////////////////////////////////

//#pragma omp parallel for
	for(int i=0;i<dim;i++)
	{
		for(int j=0;j<dim;j++)
		{
			if(dat0[i*dim+j]!=0)
			{
//#pragma omp critical
				tl0.push_back(T(i,j,dat0[i*dim+j]));
//				sm0.insert(i,j)=dat0[i*dim+j];
			}

			if(dat1[i*dim+j]!=0)
			{
//#pragma omp critical
				tl1.push_back(T(i,j,dat1[i*dim+j]));
//				sm1.insert(i,j)=dat1[i*dim+j];
			}

		}
	}

	sm0.setFromTriplets(tl0.begin(),tl0.end());
	sm1.setFromTriplets(tl1.begin(),tl1.end());

	sm0+=sm1;
	////////////////////////////////////////////////////
	cout<<"calc time: "<< getTick()-t<<endl;
	////////////////////////////////////////////////////

	cout<<"sm0 non zeros: "<<sm0.nonZeros()<<endl;
	cout<<"sm1 non zeros: "<<sm1.nonZeros()<<endl;
/*
	cout<< "Fill matrices with coefRef"<<endl;

	SpMatC16 sm2, sm3;
	sm2.resize(dim,dim);
	sm3.resize(dim,dim);
	////////////////////////////////////////////////////
	t=getTick();
	////////////////////////////////////////////////////

//#pragma omp parallel for
	for(int i=0;i<dim;i++)
	{
		for(int j=0;j<dim;j++)
		{
			if(dat0[i*dim+j]!=0)
			{
//#pragma omp critical
//				tl0.push_back(T(i,j,dat0[i*dim+j]));
				sm2.coeffRef(i,j)=dat0[i*dim+j];
			}

			if(dat1[i*dim+j]!=0)
			{
//#pragma omp critical
//				tl1.push_back(T(i,j,dat1[i*dim+j]));
				sm3.coeffRef(i,j)=dat1[i*dim+j];
			}

		}
	}
	////////////////////////////////////////////////////
	cout<<"calc time: "<< getTick()-t<<endl;
	////////////////////////////////////////////////////

	cout<<"sm2 non zeros: "<<sm2.nonZeros()<<endl;
	cout<<"sm3 non zeros: "<<sm3.nonZeros()<<endl;
*/
	// delete big matrices
	delete mat0;
	delete mat1;



	cout<<"sum matrices"<<endl;

	SpMatC16 sm01 = sm0+sm1;

	cout<<"sm01 non zeros: "<< 	sm01.nonZeros()<<endl;


	cout<<"-------- Done! -----------------"<<endl;

//	cout<<sm0<<endl;
//	cout<<sm1<<endl;
//	cout<<sm01<<endl;

}




void test_triplet()
{
	cout<<"--------Test Triplets-----------------"<<endl;




	vector<double> adns1={1,0,3,
						0,0,4,
						3,4,0};

	vector<double> adns2={0,6,0,
						0,5,4,
						0,0,0};

	SpMat sm1,sm2;

	sm1.resize(3,3);      //Change sm1 to a m x n matrix.
	sm1.reserve(3);     // Allocate room for nnz

	sm2.resize(3,3);
	sm2.reserve(3);

	typedef Eigen::Triplet<double> T;
	std::vector<T> tl1, tl2;
	tl1.reserve(3);
	tl2.reserve(3);

	for(int i=0;i<3;i++)
	{
		for(int j=0; j<3; j++)
		{
			if(adns1[i*3+j]!=0)
			{
				tl1.push_back(T(i,j,adns1[i*3+j]));
			}

			if(adns2[i*3+j]!=0)
			{
				tl2.push_back(T(i,j,adns2[i*3+j]));
			}
		}
	}

	sm1.setFromTriplets(tl1.begin(),tl1.end());
	sm2.setFromTriplets(tl2.begin(),tl2.end());


	cout<< sm1 <<endl;

	sm1.setFromTriplets(tl1.begin(),tl1.end());

	cout<< sm1 <<endl;


	//sm3.makeCompressed();


	cout<<"-------- Done! -----------------"<<endl;
}




void test_trsparse()
{
	cout<<"--------Test Triplets-----------------"<<endl;




		vector<MKL_Complex16> adns1={0,0,0,
							0,0,0,
							{0,3},0,0};

		vector<MKL_Complex16> adns2={0,0,0,
							  0,2,0,
							  {0,2},0,0};

		SpMatC16 sm1,sm2;

		sm1.resize(3,3);      //Change sm1 to a m x n matrix.
		sm1.reserve(3);     // Allocate room for nnz

		sm2.resize(3,3);
		sm2.reserve(3);

		typedef Eigen::Triplet<MKL_Complex16> T;
		std::vector<T> tl1, tl2;
		tl1.reserve(3);
		tl2.reserve(3);

		for(int i=0;i<3;i++)
		{
			for(int j=0; j<3; j++)
			{
				if(adns1[i*3+j]!=0)
				{
					tl1.push_back(T(i,j,adns1[i*3+j]));
				}

				if(adns2[i*3+j]!=0)
				{
					tl2.push_back(T(i,j,adns2[i*3+j]));
				}
			}
		}

		sm1.setFromTriplets(tl1.begin(),tl1.end());
		sm2.setFromTriplets(tl2.begin(),tl2.end());


		//sm2 = std::move( (sm1.selfadjointView<Lower>()) );

		SpMatC16 sm3 =sm1.selfadjointView<Lower>();
		SpMatC16 sm4 =sm2.selfadjointView<Lower>();

		sm3.makeCompressed();
		sm4.makeCompressed();

		cout<< sm1 <<endl;
		cout<<sm2<<endl;
		cout<< sm3 <<endl;
		cout<<sm4<<endl;

		cout<<sm3.nonZeros()<<endl;

		SpMatC16 sm5=sm3*sm4;

		cout<<sm5<<endl;
}



void test_rembo()
{
	SpMatI sm(100000,100000);

	typedef Eigen::Triplet<int> TI;
	std::vector<TI> tv;
	tv.reserve(20000);

	for(int k=0;k<10000;k++)
	{
		int i=rand()%9999 + 1;
		int j=rand()%(i-1);

		tv.push_back(TI(i,j,1));
	}

	for(int i=0;i<100000;i++)
		tv.push_back(TI(i,i,0));

	sm.setFromTriplets(tv.begin(),tv.end());



	cout<<"init "<<sm.nonZeros()<<endl;

	SpMatI sm2 = sm.selfadjointView<Lower>();

	cout<<"trang "<<sm2.nonZeros()<<endl;

	sm2.makeCompressed();

	SpMatI smnew=sm2;


	cout<<"begin"<<endl;

	SpMatI diag(100000,100000);

	long int t=getTick();

	for(int k=0; k<100; k++)
	{
		smnew=smnew * sm2;

		//smnew.diagonal()-=smnew.diagonal();

		smnew.prune([] (int i, int j, int&) { return i!=j; });

		cout<<smnew.nonZeros()<<endl;
	}

	cout<<"time: "<< (getTick()-t)/1000 <<endl;
	cout<<"end"<<endl;
}




void test_sum()
{
	omp_set_num_threads(6);
	Eigen::setNbThreads(6);

	int n=1000000;
	SpMatI sm(n,n),sm2(n,n);

	typedef Eigen::Triplet<int> TI;
	std::vector<TI> tv,tu;
	tv.reserve(n);
	tu.reserve(n);


	cout<<"init "<<endl;

	for(int k=0;k<n;k++)
	{
		int i=rand()%n;
		int j=rand()%n;

		tv.push_back(TI(i,j,rand()%1000));

		i=rand()%n;
		j=rand()%n;

		tu.push_back(TI(i,j,rand()%1000));
	}


	cout<<"set from triplets"<<endl;
	sm.setFromTriplets(tv.begin(),tv.end());
	sm2.setFromTriplets(tu.begin(),tu.end());



	cout<<"begin enw matr"<<endl;

	SpMatI diag(n,n);

	long int t=getTick();

	vector<int> nnzs(500);

	cout<<"start loop"<<endl;
	for(int k=0; k<500; k++)
	{
		diag=sm+sm2;

		diag-=sm2;
		nnzs[k]=diag.nonZeros();
	}
	cout<<endl;
	cout<<"time: "<< (getTick()-t)/1000 <<endl;
		cout<<"end"<<endl;
}



void test_map()
{
	vector<MKL_Complex16> adns1={0,1,0,
						0,0,1,
						0,0,0};

	vector<MKL_Complex16> adns2={0,0,0,
						-1,0,0,
						0,-1,0};

	vector<MKL_Complex16> v1={0,0,{0,1}};
	vector<MKL_Complex16> v2={0,{0,1},0};
	vector<MKL_Complex16> v3={0,1,0};

	Map<sps::MatrixZ> mt1(adns1.data(),3,3);
	Map<sps::MatrixZ> mt2(adns2.data(),3,3);
	Map<sps::VectorZ> mv1(v1.data(),3);
	Map<sps::VectorZ> mv2(v2.data(),3);
	Map<sps::VectorZ> mv3(v3.data(),3);

	mv1 = mt2*mv2;

	cout<<mv1<<endl;

	for(int i=0;i<3;i++) cout<<v1[i]<<" ";

	cout<<endl;


	sps::VectorZ vv;
	vv = mv2;

	MKL_Complex16 coco=(mt2*mv2).dot(mv2);

	cout<<"sds "<<mv2<<" "<< coco<<endl;

	cout<<"(0 i 0)^t * L+ =  "<<mv2.adjoint() * mt2<<endl;
	cout<<"L+ * (0 i 0)   =  "<<mt2 * mv2<<endl;

	cout<<"(0 i 0)^t * (L+)^T =  "<<mv2.adjoint() * mt2.transpose() <<endl;
	cout<<" (L+)^T * (0 i 0)   =  "<<mt2.transpose() * mv2<<endl;

	cout<<"(0 i 0)^t * L- =  "<<mv2.adjoint() * mt1<<endl;
	cout<<"L- * (0 i 0)   =  "<<mt1 * mv2<<endl;


}
