/*
 ============================================================================
 Name        : spinus_mpi.c
 Author      : Sivkov
 Version     :
 Copyright   : Your copyright notice
 Description : Compute Pi in MPI C++
 ============================================================================
 */

#include <mpi.h>
 

#include <iostream>
#include <vector>
#include <unistd.h>
#include <iomanip>
#include <time.h>
#include <cstring>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <cmath>
#include <fstream>
//-------------------------
#include "SpinSinTypes.h"
#include <mkl.h>

#include "JJState.h"
#include "Tensor.h"
#include "Hamiltonian.h"
#include "SingleTensor.h"
#include "PairTensor.h"

#include "ZeemanQOp.h"
#include "HeisenbergQOp.h"
#include "IonAnisQOp.h"
#include "SpinQOp.h"

#include "MatrixBuilder.h"
#include "SpinDynamic.h"
#include "VarParameter.h"

using namespace std;
using namespace sps;




using namespace std;
 
int main(int argc, char *argv[]) {
	int  mpi_rank, mpi_size, i;
	double PI25DT = 3.141592653589793238462643;
	double mypi, pi, h, sum, x;

	MPI::Init(argc, argv);
	mpi_size = MPI::COMM_WORLD.Get_size();
	mpi_rank = MPI::COMM_WORLD.Get_rank();

	cout<<"MPI size "<<mpi_size<<" , "<< "rank"<<mpi_rank<<endl;

	int nth = omp_get_max_threads();

	cout<<" OMP num threads " << nth << endl;


	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// make basis
	JJState jjs(3);
	jjs.PlusJ(3);
	jjs.PlusJ(3);
	//jjs.PlusJ(3);
	//jjs.PlusJ(3);
	//jjs.PlusJ(3);
	//jjs.PlusJ(3);

	jjs.BuildBasis();

	int n = jjs.getInitTwoJ().size();

	// --------------------------------------------------------------------
	// create operators
	vector<QOperator*> ops=
	{
			new HeisenbergQOp(0,1,n,-0.022),
			new HeisenbergQOp(1,2,n,-0.022),
			//new HeisenbergQOp(0,2,n,-0.022),
			//new HeisenbergQOp(2,3,n,-0.05),
			//new HeisenbergQOp(3,4,n,-0.05),
			//new HeisenbergQOp(4,5,n,-0.05),
			//new HeisenbergQOp(5,6,n,-0.05),

			new HeisenbergQOp(0,2,n, 0.01),
//			new HeisenbergQOp(0,6,n,-0.05),
//			new HeisenbergQOp(1,6,n,-0.05),
//			new HeisenbergQOp(2,6,n,-0.05),
//			new HeisenbergQOp(3,6,n,-0.05),
//			new HeisenbergQOp(4,6,n,-0.05),
//			new HeisenbergQOp(5,6,n,-0.05),

			new IonAnisQOp(0,n,-1.0,0.0),
			new IonAnisQOp(1,n,-1.0,0.0),
			new IonAnisQOp(2,n,-1.0,0.0),
			//new IonAnisQOp(3,n,-1.0,0.0),
			//new IonAnisQOp(4,n,-1.0,0.0),
			//new IonAnisQOp(5,n,-1.0,0.0),
			//new IonAnisQOp(6,n,-1.0,0.0)
	};

	vector<QOperator*> field =
	{
			//new ZeemanQOp(0,n,0.0,0.0, -2.0 * 5.8 * 0.01  ),
			new ZeemanQOp(1,n,0.0,0.0, -2.0 * 5.8 * 0.01 ),
			new ZeemanQOp(2,n,0.0,0.0, -2.0 * 5.8 * 0.01  ),
			//new ZeemanQOp(3,n,0.0,0.0, -2.0 * 5.8 * 0.01 ),
			//new ZeemanQOp(4,n,0.0,0.0, -2.0 * 5.8 * 0.01 ),
			//new ZeemanQOp(5,n,0.0,0.0, -2.0 * 5.8 * 0.01 ),
			//new ZeemanQOp(6,n,0.0,0.0, -2.0 * 5.8 * 0.01 )
	};


	vector<QOperator*> field2 =
	{
			new ZeemanQOp(0,n,0.0,0.0, -2.0 * 5.8 * 0.01 )
			//new ZeemanQOp(1,n,0.0,0.0, -2.0 * 5.8 * 0.01  ),
			//new ZeemanQOp(2,n,0.0,0.0, -2.0 * 5.8 * 0.01  ),
			//new ZeemanQOp(3,n,0.0,0.0, -2.0 * 5.8 * 0.01 ),
			//new ZeemanQOp(4,n,0.0,0.0, -2.0 * 5.8 * 0.01 ),
			//new ZeemanQOp(5,n,0.0,0.0, -2.0 * 5.8 * 0.01 )
	};


	vector<QOperator*> spinOps=
	{
			new SpinQOp(0,n),
			new SpinQOp(1,n),
			new SpinQOp(2,n)
	};


	// --------------------------------------------------------------------
	string eval_fn="eval_line_3mer.dat", mag_fn="mag_line_3mer.dat";


	// --------------------------------------------------------------------
	// create parameterization
	VarParameter mp("main"), bp("Bz field"), bp2("Bz2");
	mp.AddTensorsFromQOperator(ops);
	bp.AddTensorsFromQOperator(field);
	bp2.AddTensorsFromQOperator(field2);

	// --------------------------------------------------------------------
	// setup SD class and compute matrices
	SpinDynamic sd(&jjs);
	sd.AddVarParameter(&mp);
	sd.AddVarParameter(&bp);
	sd.AddVarParameter(&bp2);
	sd.Configure();

	// --------------------------------------------------------------------
	// get matrices for computing total matrices with parameters
	const MatrixC& mm = mp.GetMatrix();
	const MatrixC& bm = bp.GetMatrix();
	const MatrixC& bm2 = bp2.GetMatrix();

	int dim = mm.GetWidth();
	int size = mm.GetSize();

	const MKL_Complex16 *mmdat = mm.GetData();
	const MKL_Complex16 *bmdat = bm.GetData();
	const MKL_Complex16 *bmdat2 = bm2.GetData();

	MatrixC totm(dim,dim,0);

	MKL_Complex16 *totmdat = totm.GetData();

	// --------------------------------------------------------------------
	// create spin operators
	MatrixBuilder MB;
	MB.SetJJState(&jjs);

	vector<MatrixC> szms;
	for(auto sop: spinOps)
	{
		MatrixC szm;
		MB.BuildFromTensor(sop->GetComponents(2), szm);
		szms.push_back(std::move(szm));
	}


	// --------------------------------------------------------------------
	// MKL setups
	int numeig=8;
	int numeigfnd=0;
	MKL_Complex16 *eigvec = new MKL_Complex16[dim*numeig];
	int *isuppz = new int[2 * numeig];

	// --------------------------------------------------------------------
	// parameters and output arrays
	double temp = 0.025; 	//temperature in meV
	vector<double> pardat(15);
	vector<vector<double>> evs;

	vector<double> mag1(numeig);
	vector<double> mag2(numeig);
	vector<double> mag3(numeig);
	//vector<double> mag4(pardat.size());

	MKL_Complex16 *tmpvec2 = new MKL_Complex16[dim];
	
	// --------------------------------------------------------------------
	// remove old files
	std::remove(eval_fn.c_str());
	std::remove(mag_fn.c_str());


	// --------------------------------------------------------------------
	// calculation
	// --------------------------------------------------------------------
	for(int k=0;k<pardat.size();k++)
	{
		// calc total ham matrix
		pardat[k]=k*0.3+0.01;
		MKL_Complex16  par = pardat[k];
		MKL_Complex16 par2 = par + 0.001;

		cout<<"copy"<<endl;

#pragma omp parallel for simd firstprivate(par,par2)
		for(int i=0; i<size; i++)
		{
			totmdat[i] = mmdat[i] + par * bmdat[i] + par2 * bmdat2[i] ;
		}

		// eigval eigvec
		evs.push_back(vector<double>(dim));
		double *ev = evs[evs.size()-1].data();

		// --------------------------------------------------------------------
		cout<<"eig"<<endl;

		int cr = LAPACKE_zheevr( LAPACK_COL_MAJOR, 'V', 'I', 'U',
				dim, totmdat, dim, 0.0, 1.0, 1, numeig, 0.0,
				&numeigfnd, ev, eigvec, dim, isuppz );

		//int cr = LAPACKE_zheev(LAPACK_COL_MAJOR, 'V', 'U', dim, tmpdat, dim, ev);

		if(cr!=0)
			cout<<"ERROR!!"<<endl;


		// --------------------------------------------------------------------
		cout<<"moments"<<endl;
		//first mag mom
		MKL_Complex16 res;
		MKL_Complex16 alpha=1,beta=0;

		cblas_zhemv(CBLAS_ORDER::CblasColMajor, CBLAS_UPLO::CblasUpper, dim, &alpha, szms[0].GetData(), dim, eigvec, 1, &beta, tmpvec2, 1);
		cblas_zdotc_sub(dim, eigvec, 1, tmpvec2, 1, &res);
		mag1[k]=res.real();

		//sec mag mom
		cblas_zhemv(CBLAS_ORDER::CblasColMajor, CBLAS_UPLO::CblasUpper, dim, &alpha, szms[1].GetData(), dim, eigvec, 1, &beta, tmpvec2, 1);
		cblas_zdotc_sub(dim, eigvec, 1, tmpvec2, 1, &res);
		mag2[k]=res.real();

		//third mag mom
		cblas_zhemv(CBLAS_ORDER::CblasColMajor, CBLAS_UPLO::CblasUpper, dim, &alpha, szms[2].GetData(), dim, eigvec, 1, &beta, tmpvec2, 1);
		cblas_zdotc_sub(dim, eigvec, 1, tmpvec2, 1, &res);
		mag3[k]=res.real();

		cout<<mag1[k]<<" \t"<<mag2[k]<<" \t"<<mag3[k]<<" \t"<<ev[0] <<" \t"<< pardat[k]<<endl;

		// --------------------------------------------------------------------
		// output to file
		ofstream of(eval_fn,std::ofstream::out | std::ofstream::app);

		if(of.is_open())
		{
			for(int i=0;i<numeig;i++)
			{
				of<<ev[i]<<" ";
			}
			of<<endl;

			of.close();
		}
		else
		{
			cout<<"file is not opened"<<endl;
		}

		of.open(mag_fn ,std::ofstream::out | std::ofstream::app);

		if(of.is_open())
		{

			of<<mag1[k]<<" \t"<<mag2[k]<<" \t"<<mag3[k]<<" \t"<<evs[k][0] <<" \t"<< pardat[k]<<endl;


			of.close();
		}
		else
		{
			cout<<"file is not opened"<<endl;
		}
	}


	// --------------------------------------------------------------------
	// delete all
	//delete [] tmpdat;
	//delete [] tmpvec1;
	delete [] tmpvec2;
	delete [] eigvec;
	delete [] isuppz;

	for(auto op: ops)
	{
		delete op;
	}

	for(auto op: field)
	{
		delete op;
	}

	for(auto op: field2)
	{
		delete op;
	}

	for(auto op: spinOps)
	{
		delete op;
	}

	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------

	/*
	n=1000; // number of intervals

	MPI::COMM_WORLD.Bcast(&n, 1, MPI::INT, 0);
	h = 1.0 / (double) n;
	sum = 0.0;
	for (i = rank + 1; i <= n; i += size) {
		x = h * ((double) i - 0.5);
		sum += (4.0 / (1.0 + x * x));
	}
	mypi = h * sum;

	MPI::COMM_WORLD.Reduce(&mypi, &pi, 1, MPI::DOUBLE, MPI::SUM, 0);
	if (rank == 0)
		cout << "pi is approximately " << pi << ", Error is "
				<< fabs(pi - PI25DT) << endl;
	*/

	MPI::Finalize();
	return 0;
}

