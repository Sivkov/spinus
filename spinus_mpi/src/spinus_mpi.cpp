/*
 ============================================================================
 Name        : spinus_mpi.c
 Author      : Sivkov
 Version     :
 Copyright   : Your copyright notice
 Description : Compute Pi in MPI C++
 ============================================================================
 */

#include <mpi.h>
 

#include <iostream>
#include <vector>
#include <unistd.h>
#include <iomanip>
#include <time.h>
#include <cstring>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include <cmath>
#include <fstream>



#include <Game.h>

using namespace std;
using namespace sps;




using namespace std;
 
int main(int argc, char *argv[]) {
	int  mpi_rank, mpi_size, i;
	double PI25DT = 3.141592653589793238462643;
	double mypi, pi, h, sum, x;

	MPI::Init(argc, argv);
	mpi_size = MPI::COMM_WORLD.Get_size();
	mpi_rank = MPI::COMM_WORLD.Get_rank();

	cout<<"MPI size "<<mpi_size<<" , "<< "rank"<<mpi_rank<<endl;

	int nth = omp_get_max_threads();

	cout<<" OMP num threads " << nth << endl;


	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	Game *game=new Game();

	try
	{
		if(argc<2) throw SpinException("error, specify input file");

		game->Run(argv[1]);
	}
	catch(exception &ex)
	{
		cout<<ex.what()<<endl;
		std::exit(1);
	}

	delete game;

	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	MPI::Finalize();
	return 0;
}

