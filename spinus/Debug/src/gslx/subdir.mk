################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/gslx/gslx_coupling.cpp 

OBJS += \
./src/gslx/gslx_coupling.o 

CPP_DEPS += \
./src/gslx/gslx_coupling.d 


# Each subdirectory must supply rules for building sources it contributes
src/gslx/%.o: ../src/gslx/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	icc -std=c++0x -I/opt/intel/composer_xe_2013_sp1.0.080/compiler/include -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include -O0 -g3 -Wall -c -fmessage-length=0 -openmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


