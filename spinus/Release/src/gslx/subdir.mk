################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/gslx/gslx_coupling.cpp 

OBJS += \
./src/gslx/gslx_coupling.o 

CPP_DEPS += \
./src/gslx/gslx_coupling.d 


# Each subdirectory must supply rules for building sources it contributes
src/gslx/%.o: ../src/gslx/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	icc -std=c++0x -D__cplusplus=201103L -I/cluster/intel_2013SP1/composer_xe_2013_sp1.0.080/compiler/include -I/cluster/intel_2013SP1/composer_xe_2013_sp1.0.080/mkl/include -O3 -Wall -c -fmessage-length=0 -openmp -vec-report2 -xHost -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


