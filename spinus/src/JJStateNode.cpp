/*
 * JJStateNode.cpp
 *
 *  Created on: Dec 9, 2014
 *      Author: isivkov
 */

#include "JJStateNode.h"
#include <algorithm>

namespace sps
{


//-------------------------------------------------------------------------
JJStateNode::JJStateNode(int ownTwoJ)
{
	this->ownTwoJ=ownTwoJ;
	this->plusTwoJ=-1;
	this->lvlUp=nullptr;
}


//-------------------------------------------------------------------------
int JJStateNode::GetOwnTwoJ()
{
	return ownTwoJ;
}


//-------------------------------------------------------------------------
int JJStateNode::GetPlusTwoJ()
{
	return plusTwoJ;
}

//-------------------------------------------------------------------------
// could be slow, since it returns new vector of pointers
std::vector<const JJStateNode*> JJStateNode::GetSubNodes()
{
	return std::vector<const JJStateNode*>(sumTwoJ.begin(),sumTwoJ.end());
}


//-------------------------------------------------------------------------
const JJStateNode* JJStateNode::GetLvlUpNode()
{
	return lvlUp;
}


//-------------------------------------------------------------------------
std::vector<int> JJStateNode::GetJJConfiguration()
{
	std::vector<int> jjConf;

	JJStateNode *pState=this;

	while(pState!=nullptr)
	{
		jjConf.push_back(pState->ownTwoJ);

		pState=pState->lvlUp;
	}

	std::reverse( jjConf.begin(), jjConf.end() );

	return jjConf;
}


} // namespace sps
