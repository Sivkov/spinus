/*
 * math_helper.h
 *
 *  Created on: Sep 20, 2015
 *      Author: isivkov
 */


#ifndef MATH_HELPER_H_
#define MATH_HELPER_H_

#include <unistd.h>
#include <time.h>
#include <stdio.h>


#include "SpinSinTypes.h"

namespace sps
{


double GetTick(char units);


//---------------------------------------------------------------------------------------------------------------
// z= a*x+y
void sps_zaxpy(const MKL_INT n, const MKL_Complex16 a, const MKL_Complex16 *x,  MKL_Complex16 *y, MKL_Complex16 *z);

//---------------------------------------------------------------------------------------------------------------
// val = <v| H |v>
void sps_zcoovmv(MKL_INT n, MKL_Complex16 *vec, MKL_Complex16 *tmpvec, MKL_INT nnz, MKL_Complex16 *val,
		MKL_INT *indI, MKL_INT *indJ, MKL_Complex16 *res );

//---------------------------------------------------------------------------------------------------------------
double factorial(int n);

//---------------------------------------------------------------------------------------------------------------
double factorial(int n, int m);

//---------------------------------------------------------------------------------------------------------------
inline double sqrme(int twoJ, int twoK )
{
	if(2*twoJ < twoK) return 0.0;

	int K=twoK/2; // should be integer if physics is correct

	double res=1.0;

	switch(twoK)
	{
	case 0:
		res *= (twoJ + 1);
		break;

	case 2:
		res *= twoJ * (twoJ + 2) * (twoJ + 1) / 4.0;
		break;

	case 4:
		res *= ( twoJ + 3 ) * ( twoJ + 1 ) * ( twoJ + 2 ) * twoJ * ( twoJ - 1 ) / 24.0;
		break;

	default:
		int KJ = twoJ+K+1;
		double sqKf = factorial(K);
		res *= ( KJ > twoK ) ?
				( factorial(KJ,twoK) / (double)(1<<K) ) * sqKf * ( sqKf / factorial(twoJ-K) ) :
				( sqKf / factorial( twoK , KJ)) * ( sqKf / (double)(1<<K) ) / factorial(twoJ-K);
		break;
	}

	return res;
}


//---------------------------------------------------------------------------------------------------------------
double calcVonNeumanEntropy(MKL_Complex16* densityMatrix, int dim, MKL_Complex16* tmpVec);
}

#endif /* MATH_HELPER_H_ */
