/*
 * VarParameter.h
 *
 *  Created on: Jun 3, 2015
 *      Author: isivkov
 */

#ifndef VARPARAMETER_H_
#define VARPARAMETER_H_

#include <vector>
#include <string>
#include "SpinException.h"
#include "SpinSinTypes.h"
#include "QOperator.h"

namespace sps
{



class VarParameter
{
public:
	enum State
	{
		Static,
		Dynamic,
		Fixed
	};

	VarParameter(std::string name, State state = Fixed)
	:name(name), state(state), argStep(1.0){ }

	~VarParameter();

	// Tensor operations ---------------------------------------
	void AddTensor(Tensor *tensor);

	void AddTensor(const std::vector<Tensor*> &tensors);

	void AddTensorsFromQOperator(QOperator* qOp);

	void AddTensorsFromQOperator(const std::vector<QOperator*> &qOps);

	void RemoveTensors(const std::vector<Tensor*> &tensors);

	const std::vector<Tensor*> GetTensors() const;

	// Parameter Data for spin dynamics or multi static calc ----
	inline const vector<double>& GetParData(){		return parData; 	}
	inline double GetArgStep(){ return argStep; }
//	inline const vector<double>& GetArgData(){		return argData;	}
	

	void SetArgStep(double step){ argStep=step; }
	void SetParData(vector<double>&& parData){ this->parData = std::move(parData); }
//	void SetArgData(vector<double>&& argData){ this->argData = std::move(argData); }

	// you should know what you are doing
//	inline vector<double>* GetParDataUnsafe(){	return &parData;	}
//	inline vector<double>* GetArgDataUnsafe(){		return &argData;	}

	// operations with big matrix, which is a sum of all tensor matrices
	const MatrixC& SetupMatrix(long int dimension);

	inline const MatrixC& GetMatrix(){		return pTensorMatrix;	}
	inline MatrixC* GetMatrixUnsafe(){		return &pTensorMatrix;	}

	string GetName(){ return name; }

	State GetState(){	return state;	}

	void SetState(State state){	this->state=state;	}

private:
	vector<double> parData;
//	vector<double> argData;
	double argStep;
	std::vector<Tensor*> varTensors;
	std::string name;
	State state;
	MatrixC pTensorMatrix;

};

}


#endif /* VARPARAMETER_H_ */
