/*
 * Spliner.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: isivkov
 */


#include "Spliner.h"
#include "SpinException.h"

namespace sps
{
Spliner::Spliner(int order, double *dataX, double *dataY, int length)
:order(order),
 dataX(dataX),
 dataY(dataY),
 nx(length)
{
	scoeff.resize((nx-1)*order);

	xhint = DF_NO_HINT;
	yhint = DF_NO_HINT;

	ny=1; // scalar function

	/* Create a Data Fitting task */
	int status = dfdNewTask1D( &task, nx, dataX, xhint, ny, dataY, yhint );

	if(status != DF_STATUS_OK) throw SpinException("spline task is not created. ERROR "+ status);

	/* Initialize spline parameters */
	s_type = DF_PP_DEFAULT;    /* Spline is of the default type. */

	// no conditions by default
    ic_type = DF_NO_IC;
    ic = NULL;

    bc_type = DF_NO_BC;
    bc = NULL;
    scoeffhint = DF_NO_HINT;    /* No additional information about the spline. */

    /* Set spline parameters  in the Data Fitting task */
    status = dfdEditPPSpline1D( task, order, s_type, bc_type, bc, ic_type,
                                ic, scoeff.data(), scoeffhint );

    if(status != DF_STATUS_OK) throw SpinException("spline task is not created. ERROR "+ status);
}


}
