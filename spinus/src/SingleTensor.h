/*
 * SingleTensor.h
 *
 *  Created on: May 28, 2015
 *      Author: isivkov
 */

#ifndef SINGLETENSOR_H_
#define SINGLETENSOR_H_

#include "Tensor.h"
#include "SpinException.h"

namespace sps
{

class SingleTensor : public Tensor
{
public:
	SingleTensor(int numS=0,
			int numTot=1,
			int twoJ=1,
			int twoM=1,
			MKL_Complex16 factor=1.0,
			std::string name = "None")
	{
		if(numS>=numTot)
			throw SpinException("Wrong tensor parameter: numS >= numTot");

		this->M=twoM;
		this->factor=factor;
		this->name = name;

		initTwoJ = vector<int>(numTot,0);
		addTwoJ = vector<int>(numTot,0);

		for(int i=0; i<numTot; i++)
		{
			if(i==numS) initTwoJ[i] = twoJ;

			if(i>=numS) addTwoJ[i] = twoJ;
		}
	}

};
}



#endif /* SINGLETENSOR_H_ */
