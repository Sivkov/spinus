#ifndef OUTPUTDATA_H_
#define OUTPUTDATA_H_

#include <vector>



namespace sps
{

using namespace std;

struct OutputData
{
	vector<vector<double>> magMoments;
	vector<vector<double>> eigenvals;
};

}

#endif
