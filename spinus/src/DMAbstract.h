/*
 * DMAbstract.h
 *
 *  Created on: Jan 23, 2016
 *      Author: isivkov
 */

#ifndef DMABSTRACT_H_
#define DMABSTRACT_H_

#include "Eigen/Dense"
#include "SpinSinTypes.h"


using namespace std;

namespace sps
{
class DMAbstract
{
public:
	virtual ~DMAbstract(){}

	virtual void CalcDensityMatrix(MKL_Complex16 *eigenVec, int vecLength, MKL_Complex16 *outMatr)=0;

	virtual int GetDimensions()=0;

	virtual vector<int> GetSpinIndices()=0;
};

}


#endif /* DMABSTRACT_H_ */
