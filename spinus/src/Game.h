/*
 * Game.h
 *
 *  Created on: Jul 15, 2015
 *      Author: isivkov
 */

#ifndef GAME_H_
#define GAME_H_


#include <iostream>
#include <vector>
#include <unistd.h>
#include <iomanip>
#include <time.h>
#include <cstring>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include <cmath>
#include <fstream>

// important
#include "SpinSinTypes.h"
// ----------------------------
#include <mkl.h>
//-------------------------

#include "SpinStatic.h"
#include "SpinDynamic.h"

#include "JJState.h"
#include "Tensor.h"
#include "SingleTensor.h"
#include "PairTensor.h"

#include "HamiltonianBuilder.h"
#include "ZeemanQOp.h"
#include "HeisenbergQOp.h"
#include "IonAnisQOp.h"
#include "SpinQOp.h"

#include "MatrixBuilder.h"
#include "VarParameter.h"

#include "MathHelper.h"
#include "GlobalConfig.h"
#include "InputSystem/InputParser.h"

#include "StoreLocator.h"

using namespace std;

namespace sps
{

class Game
{
public:
	Game();
	~Game();

	void Run(string inputFile);

private:
	void readConfig(string inputFile);

	GlobalConfig *cfg;
};

}



#endif /* GAME_H_ */
