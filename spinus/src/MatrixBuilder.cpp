/*
 * MatrixBuilder.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: isivkov
 */


#include <math.h>
#include <iostream>
#include <mkl.h>
#include <time.h>


#include "SpinException.h"
#include "MatrixBuilder.h"

namespace sps
{

//------------------------------------------------------------------------------------------------------------------------------
MatrixBuilder::MatrixBuilder()
{
	jjState=nullptr;
	sqrRMEdone=false;
}

MatrixBuilder::MatrixBuilder(JJState *jjState)
{
	sqrRMEdone=false;
	SetJJState(jjState);
}
//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::SetJJState(JJState *jjState)
{
	if(jjState==nullptr)
		throw SpinException("SetJJState: passed in Hamiltonian jjState is NULL");

	this->jjState=jjState;
	Reset();
}




//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::Reset()
{
	jjPart.Clear();
	sqrRMEdone=false;
}



//------------------------------------------------------------------------------------------------------------------------------
double MatrixBuilder::setSqrMatrEl(const vector<int>& initStateTwoJ, const vector<int>& initTensorTwoJ)
{
	double res=1.0;

	for(int jj=0; jj<initTensorTwoJ.size(); jj++)
	{
		res*=sqrme(initStateTwoJ[jj], initTensorTwoJ[jj]);

		/*switch(initTensorTwoJ[jj])
		{
		case 0:
			res *= (initStateTwoJ[jj] + 1);
			break;

		case 2:
			res *= initStateTwoJ[jj] * (initStateTwoJ[jj] + 2) * (initStateTwoJ[jj] + 1) / 4.0;
			break;

		case 4:
			res *= ( initStateTwoJ[jj] + 3 ) * ( initStateTwoJ[jj] + 1 ) * ( initStateTwoJ[jj] + 2 ) * initStateTwoJ[jj] * ( initStateTwoJ[jj] - 1 ) / 24.0;
			break;

		default:
			throw SpinException("calcSqrReducedMatrEls: unknown rank of the Tensor");
			break;
		}
		*/
	}
//	cout<<"RME: "<<res<<endl;
	return res;
}

/*
//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::BuildFromTensor(const Tensor &tensor, MatrixC &inOutMatrix)
{



	// calc reduced matrix elements for the tensor --------
	double sqrRME = setSqrMatrEl(jjState->getInitTwoJ(), tensor.getInitTwoJ());

	// calc jj part of matrix -----------------------------
	const vector<int>& initStateTwoJ = jjState->getInitTwoJ();

	int jbSize = jjState->GetNumJStates();

	// check if jj part is already calculated
	if(tensor.jjNotEq( lastTensor ) || jjPart.GetWidth() != jbSize || jjPart.GetHeight() != jbSize)
	{
		jjPart.Clear();

		jjPart = unsafe::Matrix1D<double>(jbSize,jbSize);

		calcSqrJJHPart(tensor, sqrRME, jjPart);

		lastTensor = tensor;
	}

	// calc M part -----------------------------------------
	int mbSize = jjState->GetBasisDimension();

	if(inOutMatrix.GetWidth() != mbSize || inOutMatrix.GetHeight() != mbSize)
	{
		inOutMatrix = MatrixC(mbSize,mbSize,0);
	}

	calcMPart(tensor, inOutMatrix, jjPart);

}


//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::BuildFromTensor(const vector<Tensor*> &tensors, MatrixC &inOutMatrix)
{
	for(auto tens : tensors)
	{
		BuildFromTensor(*tens, inOutMatrix);
	}
}
*/

//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::BuildFromTensor(const Tensor &tensor, MatrixC *inOutMatrix)
{
	if(!(jjState->IsBasisBuilt()))
	{
		throw SpinException("Basis is not built");
	}
        // foolproof
        /*
         if( outHMatrix.GetHeight() != basis.GetHeight() ||
                        outHMatrix.GetWidth() != basis.GetHeight() ||
                        basis.GetWidth() != tensor.getInitTwoJ().size() + 1  ||
                        basis.GetWidth() != tensor.getAddTwoJ().size() + 1)
                throw SpinException("BuildHamiltonianFromTensor: hamiltonian matrix and basis have different dimensions");
*/


	// calc reduced matrix elements for the tensor --------
	double sqrRME = setSqrMatrEl(jjState->getInitTwoJ(), tensor.getInitTwoJ());

	// calc jj part of matrix -----------------------------
	const vector<int>& initStateTwoJ = jjState->getInitTwoJ();

	int jbSize = jjState->GetNumJStates();

	// check if jj part is already calculated
	if(tensor.jjNotEq( lastTensor ) || jjPart.GetWidth() != jbSize || jjPart.GetHeight() != jbSize)
	{
		jjPart.Clear();

		jjPart = unsafe::Matrix1D<double>(jbSize,jbSize);

		calcSqrJJHPart(tensor, sqrRME, jjPart);

		lastTensor = tensor;
	}

	// calc M part -----------------------------------------
	int mbSize = jjState->GetBasisDimension();

//	if(inOutMatrix->GetWidth() != mbSize || inOutMatrix->GetHeight() != mbSize)
//	{
//		inOutMatrix = MatrixC(mbSize,mbSize,0);
//	}

	calcMPart(tensor, inOutMatrix, jjPart);

}


//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::BuildFromTensor(const vector<Tensor*> &tensors, MatrixC *inOutMatrix)
{
	for(auto tens : tensors)
	{
		BuildFromTensor(*tens, inOutMatrix);
	}
}

void MatrixBuilder::calcNZjjPart(const Tensor &tensor, TripletVectorD &jjTriplets)
{
	const unsafe::Matrix1D<int>& jjBasis = jjState->GetJJBasis();

	// get sizes of basis and jjconf
	long int jConfSize = jjBasis.GetWidth();
	long int size = jjBasis.GetHeight();

	// get pointer to basis data
	const int *bDataLeft = jjBasis.GetData();
	const int *bDataRight = jjBasis.GetData();

	// add conf of the tensor
	const int *tensorAddJConf = tensor.getAddTwoJ().data();
	const int* stateInitJConf = jjState->getInitTwoJ().data();
	const int *tensorInitJConf = tensor.getInitTwoJ().data();

	double sqReducedME = setSqrMatrEl(jjState->getInitTwoJ(), tensor.getInitTwoJ());



	tbb::parallel_for(long(0), size, [&]( long i )
//#pragma omp parallel for shared(jjTriplets) //reduction(merge: jjTriplets)
//	for(int i = 0; i < size; i++)
	{
		const int *jConfLeft =  &bDataLeft[i * jConfSize];

		for(int j = 0; j < size; j++)
		{
			const int *jConfRight =  &bDataRight[ j * jConfSize];

			double coef = sqrt(calcSqrJJME(tensorAddJConf, jConfLeft, jConfRight, jConfSize) * sqReducedME)*
					calc9jME(tensorInitJConf, tensorAddJConf, stateInitJConf, jConfLeft, jConfRight, jConfSize) ;

			if(coef != 0.0 )
			{
//#pragma omp critical
				jjTriplets.push_back( TripletD(i,j, coef) );
			}
		}
	}
	);

}


//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::calcMPartSparse(const Tensor &tensor, TripletVectorZ &outTriplets, const TripletVectorD &jjTriplets)
{
	long int bSize = jjState->GetBasisDimension();

	// get jj-to-m connection basis
	const vector<long int> &JtoMConnection=jjState->GetJtoMBasisConnection();

	// get jj basis
	const unsafe::Matrix1D<int> &jjBasis=jjState->GetJJBasis();
	const int *jjData=jjBasis.GetData();

	// sizes of jj basis
	int jjSize = jjBasis.GetHeight();
	int jConfSize = jjBasis.GetWidth();

	// full basis
	const int* fullBasisData = jjState->GetFullBasis().GetData();
	long int fullBasisSize = jjState->GetBasisDimension();

	// parameters of tensor
	int tensorJ=tensor.getAddTwoJ()[jConfSize-1];
	int tensorM=tensor.getM();

	MKL_Complex16 tFactor = tensor.getFactor();

	// loop, may be need parallelize. One need to test push_back with it
	long int jjTripletsLength = jjTriplets.size();



	tbb::parallel_for(long(0), jjTripletsLength, [&]( long jj )
//	for(int jj=0; jj< jjTripletsLength; jj++)
	{
		// get indices from jj triplet
		int i = jjTriplets[jj].row();
		int j = jjTriplets[jj].col();

		// get value of matrix element from triplet
		double jjPartVal = jjTriplets[jj].value();

		// get J numbers of main angular moment
		int leftStateJ =  jjData[(i+1)*jConfSize-1];
		int rightStateJ =  jjData[(j+1)*jConfSize-1];

		// get start index for M projection values in full basis array from j-to-m vector
		long int startFullBasisI = JtoMConnection[i];
		long int startFullBasisJ = JtoMConnection[j];

		// calc jm matrix elements and fill triplet array
		for(int m1=0; m1 <  leftStateJ + 1; m1++)
		{
			for(int m2=0; m2 < rightStateJ + 1; m2++)
			{
				// absolute matrix indices
				int absI = startFullBasisI + m1;
				int absJ = startFullBasisJ + m2;

				// m components of basis J states
				int ml = fullBasisData[ 2 * absI + 1];
				int mr = fullBasisData[ 2 * absJ + 1];

				// -1 or +1 coef
				int coef = (((leftStateJ - ml)>>1)&1) ? -1.0 : 1.0;

				// calc matrix element
				double s3j = gsl_sf_coupling_3j(leftStateJ, tensorJ, rightStateJ, -ml, tensorM, mr);
//cout<<leftStateJ<<" "<< tensorJ <<" "<<  rightStateJ<<"\n"<< -ml<<" "<<tensorM<<" "<< mr <<"\n--------"<<endl;
				// add new triplet
				if(s3j != 0.0)
				{
//#pragma omp critical
					outTriplets.push_back( TripletZ(absI, absJ, tFactor * coef * jjPartVal * s3j) );
				}
			}
		}
	}
	); // parallel_for
}


//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::BuildFromTensorSparse(const Tensor &tensor, SparseMatrixZ &inOutMatrix)
{
	if(!(jjState->IsBasisBuilt()))
	{
		throw SpinException("Basis is not built");
	}

	// calc
	TripletVectorD jjTriplets;

	jjTriplets.reserve(jjState->GetBasisDimension() * 30); // * 10 is empirical


	calcNZjjPart(tensor, jjTriplets);

	TripletVectorZ jmTriplets;

	jmTriplets.reserve(jjState->GetBasisDimension() * 300); // * 100 is empirical

	calcMPartSparse(tensor,jmTriplets,jjTriplets);

//	cout<<"JJsie: "<<jjTriplets.size()<<endl;
	inOutMatrix.resize(jjState->GetBasisDimension(),jjState->GetBasisDimension());

	inOutMatrix.setFromTriplets(jmTriplets.begin(),jmTriplets.end());

}

//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::calcSqrJJHPart(const Tensor &tensor, double sqReducedME, unsafe::Matrix1D<double> &outSqrJJPart)
{
	const unsafe::Matrix1D<int>& jjBasis = jjState->GetJJBasis();

	// get sizes of basis and jjconf
	long int jConfSize = jjBasis.GetWidth();
	long int size = jjBasis.GetHeight();

	// get pointer
	double *data = outSqrJJPart.GetData();

	// get pointer to basis data
	const int *bDataLeft = jjBasis.GetData();
	const int *bDataRight = jjBasis.GetData();

	// add conf of the tensor
	const int *tensorAddJConf = tensor.getAddTwoJ().data();
	const int* stateInitJConf = jjState->getInitTwoJ().data();
	const int *tensorInitJConf = tensor.getInitTwoJ().data();

	// create 9j matrix
	unsafe::Matrix1D<double> jj9jPart(size,size);
	double *data9j = jj9jPart.GetData();


#pragma omp parallel for schedule(dynamic,10)
	for(int i = 0; i < size; i++)
	{
		const int *jConfLeft =  &bDataLeft[i * jConfSize];

		for(int j = 0; j <= i; j++)
		{
			const int *jConfRight =  &bDataRight[ j * jConfSize];

			data[i*size+j] = sqrt(calcSqrJJME(tensorAddJConf, jConfLeft, jConfRight, jConfSize) * sqReducedME)*
					calc9jME(tensorInitJConf, tensorAddJConf, stateInitJConf, jConfLeft, jConfRight, jConfSize);
		}
	}
}



//------------------------------------------------------------------------------------------------------------------------------
void MatrixBuilder::calcMPart(const Tensor &tensor, MatrixC *ham, const unsafe::Matrix1D<double> &jjPart)
{
	long int bSize = jjState->GetBasisDimension();

	const unsafe::Matrix1D<int> &mBasis=jjState->GetFullBasis();
	const unsafe::Matrix1D<int> &jjBasis=jjState->GetJJBasis();

	int jjSize = jjState->GetNumJStates();
	int jConfSize = jjBasis.GetWidth();

	long int ind=0;

	MKL_Complex16 *mData = ham->GetData();
	const int *mbData=mBasis.GetData();
	const int *jjData=jjBasis.GetData();

	int tensorJ=tensor.getAddTwoJ()[jConfSize-1];
	int tensorM=tensor.getM();

	const double *jjPartData = jjPart.GetData();

	MKL_Complex16 tFactor = tensor.getFactor();

#pragma omp parallel for schedule(dynamic,100)
	for(long int j1=0;j1<bSize; ++j1)
	{
		int jIndl = mbData[j1*2];
		int ml=mbData[j1*2+1];

		int leftStateJ =  jjData[(jIndl+1)*jConfSize-1];

		double coef = (((leftStateJ - ml)>>1)&1) ? -1.0 : 1.0;

		for(long int j2=0;j2<=j1; ++j2)
		{
			if( tensorM + mbData[j2*2+1] != ml)
				continue;

			int jIndr = mbData[j2*2];
			int rightStateJ =  jjData[(jIndr+1)*jConfSize-1];
			double jjPartVal = jjPartData[jIndl*jjSize+jIndr];

			if(jjPartVal==0)
				continue;

			double s3j = gsl_sf_coupling_3j(leftStateJ, tensorJ, rightStateJ, -ml, tensorM, mbData[j2*2+1]);

			mData[j1*bSize+j2] += tFactor * coef * jjPartVal * s3j;


		}
	}
}




}

