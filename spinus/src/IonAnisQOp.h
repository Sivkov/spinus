/*
 * IonAnisQOp.h
 *
 *  Created on: Jun 14, 2015
 *      Author: isivkov
 */

#ifndef IONANISQOP_H_
#define IONANISQOP_H_

#include "QOperator.h"
#include "SingleTensor.h"

namespace sps
{

class IonAnisQOp : public QOperator
{
public:
	IonAnisQOp(int numS=0, int numTot=1, double D=1.0, double E=1.0, string name="")
	: QOperator(name), D(D), E(E)
	{
		tensors = std::vector<Tensor*>(3,nullptr);

		MKL_Complex16 im = {0,1};

		if( D!=0 )
		{
			tensors[0] = new SingleTensor(numS, numTot, 4, 0, std::sqrt(2.0/3.0) * D, "ionanis Dz");
		}

		if( E!=0 )
		{
			tensors[1] = new SingleTensor(numS, numTot, 4,  4, E, "ionanis Exy 2");
			tensors[2] = new SingleTensor(numS, numTot, 4, -4, E, "ionanis Exy -2");
		}

		// add new tensors in store
		StoreLocator<Tensor>::GetStore()->Add(tensors);
	}

	virtual std::vector<Tensor*> GetComponents(int num)
		{
			switch(num)
			{
			case 0:
				return std::vector<Tensor*>(tensors.begin(), tensors.begin()+1 );
				break;

			case 1:
				return std::vector<Tensor*>(tensors.begin()+1, tensors.begin()+3 );
				break;

			default:
				return std::vector<Tensor*>();
				break;
			}
		}

protected:

	double D,E;
};
}


#endif /* IONANISQOP_H_ */
