/*
 * SpinStatic.cpp
 *
 *  Created on: Jul 10, 2015
 *      Author: isivkov
 */

#include <iostream>

#include "SpinStatic.h"

using namespace std;

namespace sps
{

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
SpinStatic::SpinStatic():
		hamBuilder(nullptr),
		jjState(nullptr),
		numeigfnd(0),
		basisDim(0),
		configured(false),
		parLoopIndex(0),
		Zsum(0),
		numOutSpins(0),
		numOutSComps(0)
{ }


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
SpinStatic::~SpinStatic()
{
	for(auto so: spinOps)
	{
		delete so;
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::SetJJState(JJState *jjs)
{
	jjState=jjs;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::SetHamBuilder(HamiltonianBuilder *hb)
{
	hamBuilder=hb;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::SetConfig(SpinStaticConfig &config)
{
	this->config=config;
	configured=true;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::SetConfig(SpinStaticConfig &&config)
{
	this->config=config;
	configured=true;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::Calc()
{
	if( !(hamBuilder->GetConfigured()) )
	{
		throw SpinException("Hamiltonian is not configured");
	}

	//-----------------------------------------------------------------
	// prepare shit
	//-----------------------------------------------------------------
	numOutSpins = config.spinNumsForMagMom.size();
	numOutSComps = config.spinComponents.size();
	basisDim = jjState->GetBasisDimension();

	// get params
	vector<VarParameter*> &params = *(hamBuilder->GetVarParams());

	// basic arrays init
	curParams.resize(params.size());
	//parMatDatas.resize(params.size());
	curEigVals.resize(basisDim);
	curEigVecs.resize(basisDim * config.numEig);
	tmpvec2.resize(basisDim);
	isuppz.resize(2 * config.numEig);
	Zfactors.resize(config.numEig);

	// to store total matrix after summation with parameters
	totmat=MatrixC(basisDim,basisDim,0);

	//get params and store max pardata length
	// and store data for quicker access
	int maxParDataSize=1;
	for(int i = 0; i< params.size(); i++)
	{
		const vector<double>& pardata = params[i]->GetParData();

		if(pardata.size()>maxParDataSize)
		{
			maxParDataSize = pardata.size();
		}

		// store matrix data
		MKL_Complex16 *data=params[i]->GetMatrixUnsafe()->GetData();
		parMatDatas.push_back(data);
	}


	// prepare for spin operators matrices -------------------------
	magMomInit();

	// setup some output
	outputInit();

	// user
	userInit();

	//-----------------------------------------------------------------
	// main loop
	//-----------------------------------------------------------------
	for(int k=0;k<maxParDataSize; k++)
	{
		parLoopIndex = k;

		cout<<"--------- Parameter step: "<<k<<"  ---------------"<<endl;

		// setup parameters for this iteration
		for(int i = 0; i< params.size(); i++)
		{
			const vector<double>& pardata = params[i]->GetParData();

			int psize = pardata.size();

			if(psize > k)
			{
				curParams[i] = pardata[k];
			}


			if(psize==0 || params[i]->GetState() == VarParameter::Fixed )
			{
				curParams[i]=1.0;
			}
		}


		//calc total matrix
		cout<<"calc total matrix"<<endl;
		calcTotalMatrix();

		//find needed eigvals and/or vectors
		cout<<"find eigenvals"<<endl;
		eigSolve();

		//calc magn moments
		cout<<"calc magmoms"<<endl;
		calcMagMom();

		//save eigvals
		cout<<"save eigval"<<endl;
		outputEigVal();

		//save moments
		cout<<"save magmom"<<endl;
		outputMagMom();

		// user shit

		userCalc();
	}

	cout<<"Done!"<<endl;
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::userInit()
{

}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::userCalc()
{

}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::outputInit()
{
	// eval file just remove
	std::remove(config.eigValOutFile.c_str());

	// magmom file reopen and write head line
	ofstream magf(config.magMomOutFile,std::ofstream::out);

	// moment output
	for(auto mom: config.spinNumsForMagMom )
	{
		for(int j=0; j<config.spinComponents.size(); j++)
		{
			string pref="";

			switch (config.spinComponents[j])
			{
				case 0:
				{
					pref="x";
				}break;

				case 1:
				{
					pref="y";
				}break;

				case 2:
				{
					pref="z";
				}break;
			}

			// write spin nums which constitute current moment
			magf <<"S";
			for(auto snum: mom)
				magf<<snum;

			magf<<"_"<<pref<<"\t";
		}
	}

	magf<<endl;

	magf.close();


	if(config.writeAllMagMom)
	{
		ofstream magf(config.magMomPerEigFile,std::ofstream::out);

		for(auto mom: config.spinNumsForMagMom )
		{
			for(int j=0; j<config.spinComponents.size(); j++)
			{
				for(int state=0; state < config.numEig; state++)
				{
					string pref="";

					switch (config.spinComponents[j])
					{
						case 0:
						{
							pref="X";
						}break;

						case 1:
						{
							pref="Y";
						}break;

						case 2:
						{
							pref="Z";
						}break;
					}

					// write spin nums which constitute current moment
					magf <<"S";
					for(auto snum: mom)
						magf<<snum;

					magf<<"_"<<pref<<"_eig"<<state<<"\t";
				}
			}
		}

		magf<<endl;

		magf.close();
	}

}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::outputEigVal()
{
	ofstream of(config.eigValOutFile,std::ofstream::out | std::ofstream::app);

	if(of.is_open())
	{
		for(int i=0;i<numeigfnd;i++)
		{
			of<<curEigVals[i]<<"\t";
		}
		of<<endl;

		of.close();
	}
	else
	{
		cout<<"eig file is not opened"<<endl;
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::outputMagMom()
{
	ofstream of(config.magMomOutFile ,std::ofstream::out | std::ofstream::app);

	if(of.is_open())
	{
		for(int i=0;i<numOutSpins*numOutSComps; i++)
		{
			of <<curMagMoms[i]<<"\t";
		}

		of<<endl;

		of.close();
	}
	else
	{
		cout<<"file is not opened"<<endl;
	}


	if(config.writeAllMagMom)
	{
		// magmoms per eigenstate
		ofstream of(config.magMomPerEigFile ,std::ofstream::out | std::ofstream::app);

		if(of.is_open())
		{
			for(int i=0;i<numOutSpins*numOutSComps*config.numEig; i++)
			{
				of <<curMagMomsEig[i]<<"\t";
			}

		of<<endl;
		of.close();
		}
		else
		{
			cout<<"file is not opened"<<endl;
		}
	}

}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::calcTotalMatrix()
{
	MKL_Complex16 *totmdat = totmat.GetData();

	int parsize = curParams.size();

	long int msize = totmat.GetSize();

#pragma omp parallel for //simd firstprivate(par,par2)
	for(int i=0; i<msize; i++)
	{
		totmdat[i]=0.0;

//#pragma simd
		for(int j=0; j < parsize; j++ )
		{
			totmdat[i] += curParams[j] * parMatDatas[j][i];
		}

	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
double SpinStatic::calcZFactor(double energy, double temp)
{
	if(temp == 0.0000000 && std::abs(energy) != 0.00000000)
		return 0.0;

	if(temp == 0.00000000 && std::abs(energy) == 0.00000000)
		return 1.0;

	return std::exp( energy / config.temperature );
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::eigSolve()
{
	MKL_Complex16 *totmdat = totmat.GetData();
	double *ev = curEigVals.data();

	int cr = LAPACKE_zheevr( LAPACK_COL_MAJOR, 'V', 'I', 'U',
			basisDim, totmdat, basisDim, 0.0, 1.0, 1, config.numEig, 0.0,
			&numeigfnd, ev, curEigVecs.data(), basisDim, isuppz.data() );

	if(cr!=0)
		cout<<"ERROR in eigenvals!!"<<endl;

	// calc temperature distribution
	Zsum=0.0;
	for(int i=0; i< numeigfnd; i++)
	{
		Zfactors[i] = calcZFactor( -(curEigVals[i]-curEigVals[0]) , config.temperature );
		Zsum += Zfactors[i];
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::magMomInit()
{
	int n = jjState->getInitTwoJ().size();

	// add spin ops
	spinMatBldr.SetJJState(jjState);
	spinmat = MatrixC(basisDim,basisDim,0);

	for(int i=0;i<n; i++)
	{
		spinOps.push_back(new SpinQOp(i,n));
	}

	//init magmom array
	curMagMoms.resize(numOutSpins*numOutSComps, 0 );
	curMagMomsEig.resize(numOutSpins*numOutSComps*config.numEig,0 );
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
bool SpinStatic::calcSpinOpMatr(int num, int component, MatrixC &spinMat)
{
	if( component<0 || component >2 || num <0 || num >= spinOps.size())
		return false;

	MKL_Complex16 *data = spinMat.GetData();
	memset((void*)data, 0, sizeof(MKL_Complex16)*spinMat.GetSize());

	spinMatBldr.BuildFromTensor(spinOps[num]->GetComponents(component),&spinMat);

	return true;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
bool SpinStatic::calcSpinOpMatr(const vector<int> &spinNums, int component, MatrixC &spinMat)
{
	if( component<0 || component >2 )
		return false;

	MKL_Complex16 *data = spinMat.GetData();
	memset((void*)data, 0, sizeof(MKL_Complex16)*spinMat.GetSize());

	vector<Tensor*> spinTensors;

	for(auto snum: spinNums)
	{
		if(snum<0 || snum >= spinOps.size()) return false;

		vector<Tensor*> tensors = spinOps[snum]->GetComponents(component);
		spinTensors.insert(spinTensors.end(),tensors.begin(),tensors.end());
	}

	spinMatBldr.BuildFromTensor(spinTensors,&spinMat);

	return true;
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinStatic::calcMagMom()
{
	MKL_Complex16 res;
	MKL_Complex16 alpha=1,beta=0;

	MKL_Complex16 *tmdata = tmpvec2.data();

	for(int j=0; j<numOutSpins; j++) // over spins
	{
		for(int c=0; c<numOutSComps; c++) // over spin components
		{
			int comp = config.spinComponents[c];
			const vector<int> &snums = config.spinNumsForMagMom[j];

			if( !calcSpinOpMatr(snums,comp, spinmat) ) continue;

			MKL_Complex16 *eigvecdata = curEigVecs.data();

			int ind = numOutSComps * j + c;

			curMagMoms[ind] = 0;

//#pragma omp parallel for
			for(int i=0; i<numeigfnd; i++) // over eigenvals
			{
				cblas_zhemv(CBLAS_ORDER::CblasRowMajor, CBLAS_UPLO::CblasLower, basisDim, &alpha, spinmat.GetData(), basisDim, eigvecdata, 1, &beta, tmdata, 1);
				cblas_zdotc_sub(basisDim, eigvecdata, 1, tmdata, 1, &res);

				int ind2 = ind * numeigfnd + i;

				curMagMomsEig[ind2] = res.real();// * Zfactors[i] / Zsum;

//#pragma omp atomic
				curMagMoms[ind] += res.real() * Zfactors[i] / Zsum;

				eigvecdata+=basisDim;
			}
		}
	}
}




}

