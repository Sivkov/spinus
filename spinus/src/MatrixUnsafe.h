/*
 * Matrix.h
 *
 *  Created on: Dec 12, 2014
 *      Author: isivkov
 */

#ifndef MATRIX_UNSAFE_H_
#define MATRIX_UNSAFE_H_

#include <cstring>

#include "VectorUnsafe.h"

namespace sps
{

namespace unsafe
{

template<class type>
class Matrix1D : public Vector<type>
{
public:
	Matrix1D(){this->data=nullptr; height=0; width=0; this->dontFree=false;}
	Matrix1D(long int width, long int height);
	Matrix1D(long int width, long int height, int setVal);
	Matrix1D(type *data, long int width, long int height, bool dontFree=false);

	virtual ~Matrix1D(){};

	long int GetWidth() const;
	long int GetHeight() const;

	virtual void Clear();

protected:
	long int width, height;
};



	//-------------------------------------------------------------------------
	template<class type>
	Matrix1D<type>::Matrix1D(long int width, long int height)
	:Vector<type>(height*width), height(height), width(width)
	{
	}


	//-------------------------------------------------------------------------
	template<class type>
	Matrix1D<type>::Matrix1D(long int width, long int height, int setVal)
	:Vector<type>(height*width), height(height), width(width)
	{
		memset((void*)(Vector<type>::data), setVal, Vector<type>::size*sizeof(type));
	}

	//-------------------------------------------------------------------------
	template<class type>
	Matrix1D<type>::Matrix1D(type *data, long int width, long int height, bool dontFree)
	:Vector<type>(data, length*width, dontFree), height(height), width(width)
	{
	}


	//-------------------------------------------------------------------------
	template<class type>
	void Matrix1D<type>::Clear()
	{
		Vector<type>::Clear();
		this->width = 0;
		this->height = 0;
	}

	//-------------------------------------------------------------------------
	template<class type>
	long int Matrix1D<type>::GetWidth() const
	{
		return width;
	}

	//-------------------------------------------------------------------------
	template<class type>
	long int Matrix1D<type>::GetHeight() const
	{
		return height;
	}



}

}





#endif /* MATRIX_UNSAFE_H_ */
