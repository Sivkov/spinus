/*
 * QStateManager.cpp
 *
 *  Created on: Dec 8, 2014
 *      Author: isivkov
 */

#include <cmath>
#include <stdlib.h>
#include <utility>
#include <cstring>
#include <iostream>
#include <ctime>


#include "JJState.h"
#include "SpinException.h"

namespace sps
{



//-------------------------------------------------------------------------
JJState::JJState(int initTwoJ)
{
	basisDimension=initTwoJ+1;
	numJStates=1;

	stateTree=new JJStateNode(initTwoJ);
	statesStore.push_back(stateTree);
	initJ.push_back(initTwoJ);
	isBasisBuilt=false;
	numParticles=1;
}

//-------------------------------------------------------------------------
JJState::~JJState()
{
	for(int i=0;i<statesStore.size();i++)
	{
		delete statesStore[i];
	}
}

//-------------------------------------------------------------------------
void JJState::PlusJ(int twoAddJ)
{
	initJ.push_back(twoAddJ);
	basisDimension*=twoAddJ+1;
	numJStates=0;
	plusJ(stateTree,twoAddJ);
	numParticles++;
}

//-------------------------------------------------------------------------
void JJState::PlusJ(std::vector<int> &twoJList)
{
	// the code could be with parallelization
}



void JJState::plusJ(JJStateNode* stateNode, int twoAddJ)
{
	if(stateNode==nullptr) // shouldn't be
		return;

	// the state is at end (was not summed before) ?
	if(stateNode->plusTwoJ==-1)
	{
		// make summation of moments
		stateNode->plusTwoJ=twoAddJ;

		int size=(2 + twoAddJ+stateNode->ownTwoJ - abs(twoAddJ-stateNode->ownTwoJ))/2;

		// update counter of J states
		numJStates+=size;

		// prepare array for resulting J12 states (J1+J2=J12)
		stateNode->sumTwoJ.reserve(size);

		// fill array with resulting J12
		for(int j = abs(twoAddJ-stateNode->ownTwoJ);
				j <= twoAddJ+stateNode->ownTwoJ;
				j += 2)
		{
			JJStateNode *pNewState=new JJStateNode(j);

			pNewState->lvlUp=stateNode;

			stateNode->sumTwoJ.push_back(pNewState);

			statesStore.push_back(pNewState);
		}
	}
	else
	{
		// pass new J further to find end state and sum
		// this can be parallelized
		for(auto state: stateNode->sumTwoJ)
		{
			plusJ(state,twoAddJ);
		}
	}
}



const std::vector<int>& JJState::getInitTwoJ() const
{
	return initJ;
}



//-------------------------------------------------------------------------
long int JJState::GetNumJStates() const
{
	return numJStates;
}


//-------------------------------------------------------------------------
long int JJState::GetBasisDimension() const
{
	return basisDimension;
}


//-------------------------------------------------------------------------
void JJState::extractLastJJNodes(JJStateNode *pState, std::vector<JJStateNode*> &store)
{
	if(pState->plusTwoJ==-1)
	{
		store.push_back(pState);
		return;
	}

	for(auto sumj: pState->sumTwoJ)
	{
		if(sumj->plusTwoJ==-1)
		{
			store.push_back(sumj);
		}
		else
		{
			extractLastJJNodes(sumj,store);
		}
	}
}




//-------------------------------------------------------------------------
void JJState::BuildBasis()
{
	//separateBasis.clear();
	// get all end states from state tree
	lastJJNodes.clear();
	extractLastJJNodes(stateTree,lastJJNodes);

	// get size of jjConf array and size of extracted end state array
	long int jjbSize=lastJJNodes.size();
	int jjConfSize=initJ.size();
	
	// if they are not equal to internal test values, then error
	if(jjbSize!=numJStates)
		throw SpinException("JJState invalid, number of JJ states is not equal to tested number of JJ states");

	if(jjConfSize!=lastJJNodes[0]->GetJJConfiguration().size())
		throw SpinException("JJState invalid, jj state conf size is not equal to tested jj state conf size");

	jjBasis = unsafe::Matrix1D<int>(jjConfSize,jjbSize);

	// temp variables
	int twoJ;
	int *jjData=jjBasis.GetData();

	// M parted basis
	mBasis.resize(jjbSize);

	// full M basis
	fullBasis=unsafe::Matrix1D<int>(2,basisDimension);

	int *fbData=fullBasis.GetData();

	int ind=0;
	int mInd=0;


	double start = omp_get_wtime();

	//omp_set_nested(0);
	// fill jj conf array
//#pragma omp parallel for reduction(+:ind)
	for(long int jj=0; jj<jjbSize; ++jj)
	{
		std::vector<int> jjConf = lastJJNodes[jj]->GetJJConfiguration();

#pragma simd
		for(int j = 0; j< jjConfSize; ++j)
		{
			jjData[jjConfSize*jj+j] = jjConf[j];
		}

		int mSize = jjConf[jjConfSize-1] + 1;


#pragma simd
		for(int m = 0; m< mSize; m++)
		{
			int mom = -mSize + 1 + m*2;

			fbData[2*m+ind] = jj;
			fbData[1+2*m+ind] = mom;


		}

		// fill JJ to FullBasis connection
		mBasis[jj] = mInd;

		mInd+=mSize;
		ind+=2*mSize;
	}

	cout<<endl;
	double end = omp_get_wtime();
	cout<<"Basis build time "<<(end-start)<<endl;

	cout<<"total J states: "<<GetNumJStates()<<std::endl;
	cout<<"basis dimension (JM states): "<<GetBasisDimension()<<std::endl;

	isBasisBuilt=true;
}



//-------------------------------------------------------------------------
const unsafe::Matrix1D<int>& JJState::GetFullBasis() const
{
	return fullBasis;
}

//-------------------------------------------------------------------------
const unsafe::Matrix1D<int>& JJState::GetJJBasis() const
{
	return jjBasis;
}



//-------------------------------------------------------------------------
const vector<long int>& JJState::GetJtoMBasisConnection() const
{
	return mBasis;
}




}

