/*
 * helper.h
 *
 *  Created on: Sep 30, 2015
 *      Author: isivkov
 */

#ifndef HELPER_H_
#define HELPER_H_

#include <string>
#include <algorithm>
#include <ctype.h>

namespace sps
{

template<typename type>
type stonum(std::string s);

template<>int stonum(std::string s)
{
	return std::stoi(s);
}

template<>double stonum(std::string s)
{
	return std::stod(s);
}

std::string ToLower(std::string str);

template<>bool stonum(std::string s)
{
	return (s=="0" || ToLower(s)=="false" || ToLower(s)=="f") ? false: true ;
}


}



#endif /* INPUTSYSTEM_HELPER_H_ */
