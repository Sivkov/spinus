/*
 * MatrixBuilder.h
 *
 *  Created on: Jun 11, 2015
 *      Author: isivkov
 */

#ifndef MATRIXBUILDER_H_
#define MATRIXBUILDER_H_

#include <vector>
#include <stdio.h>



#include "gsl/gsl_math.h"
#include "gsl/gsl_sf.h"

#include "JJState.h"
#include "Tensor.h"
#include "VectorUnsafe.h"
#include "MatrixUnsafe.h"
#include "gslx/gslx_coupling.h"
#include "SpinSinTypes.h"
#include "MathHelper.h"

#include "Eigen/SparseCore"
#include "Eigen/Dense"

#include <tbb/tbb.h>
#include <tbb/concurrent_vector.h>

using namespace std;

namespace sps
{





class MatrixBuilder
{
public:
	MatrixBuilder();

	MatrixBuilder(JJState *jjState);

//	void BuildFromTensor(const Tensor &tensor, MatrixC &inOutMatrix);

//	void BuildFromTensor(const vector<Tensor*> &tensors, MatrixC &inOutMatrix);

	void BuildFromTensor(const Tensor &tensor, MatrixC *inOutMatrix);

	void BuildFromTensor(const vector<Tensor*> &tensors, MatrixC *inOutMatrix);

	void BuildFromTensorSparse(const Tensor &tensor, SparseMatrixZ &inOutMatrix);

	void SetJJState(JJState *jjState);

	void Reset();

	// Sparse
	void calcNZjjPart(const Tensor &tensor, TripletVectorD &jjTriplets);

	void calcMPartSparse(const Tensor &tensor, TripletVectorZ &outTriplets, const TripletVectorD &jjTriplets);

private:

	// Dense
	void calcJJHMatr(const Tensor &tensor, unsafe::Matrix1D<double> &jjHam);

	void calcSqrJJHPart(const Tensor &tensor, double sqReducedME, unsafe::Matrix1D<double> &outSqrJJPart);

	void calc9jJJHPart(const Tensor &tensor, unsafe::Matrix1D<double> &out9jJJPart);

	void calcMPart(const Tensor &tensor, MatrixC *ham, const unsafe::Matrix1D<double> &jjPart);

	double setSqrMatrEl(const vector<int>& initStateTwoJ, const vector<int>& initTensorTwoJ);

	void buildFromTensorAndBasis(const Tensor& tensor, double sqReducedME,
			const unsafe::Matrix1D<int>& basis, const vector<int>& initStateTwoJ,
			unsafe::Matrix1D<double>& outHMatrix);

	void calcSqrReducedMatrEl();

	//---------------------------------------------------------------------------------------------------------------
	inline double calcSqrJJME(const int *tensorAddConf, const int *leftAddConf, const int *rightAddConf, int confSize)
	{
		double sqrFactor=1.0;

		// calc sqr of factor like [k][S1][S2]
		for(int i=1; i<confSize; i++)
		{
			sqrFactor*=( tensorAddConf[i] + 1 ) * ( rightAddConf[i] + 1 ) * ( leftAddConf[i] + 1 );
		}

		return sqrFactor; // /(double)(leftAddConf[confSize-1]+1);
	}

	//---------------------------------------------------------------------------------------------------------------
	inline double calc9jME(const int *tensorInitConf, const int *tensorAddConf, const int *initConf, const int *leftAddConf, const int *rightAddConf, int confSize)
	{
		double res=1.0;

		bool ret=false;

#pragma simd
		for(int i=0; i<confSize; i++)
		{
			int r1=abs(leftAddConf[i]-tensorAddConf[i])-rightAddConf[i];
			int r2=leftAddConf[i]+tensorAddConf[i]-rightAddConf[i];

			if( r1 > 0 ||r2 < 0 )
				ret=true;
		}


		if(ret)
			return 0.0;

																		  // calc {9j}
		for(int i=1; i<confSize; i++)
		{

			res *= gsl_sf_coupling_9j( tensorAddConf[i-1], tensorInitConf[i], tensorAddConf[i],
											 leftAddConf[i-1],	 initConf[i],		leftAddConf[i],
											 rightAddConf[i-1],	 initConf[i],		rightAddConf[i]	 );
		}

		return res;
	}


	//---------------------------------------------------------------------------------------------------------------

	// list of tensor sqr parts of init JJ conf
	vector<long int> reducedAnisoSqrMatrEls;

	// full matrix of sqr part of JJ - for anisotropic calculation
	unsafe::Matrix1D<double> jjPart;

	// more convenient
//	vector<TripletD> nzJJPart;

	JJState *jjState;

	Tensor lastTensor;



	bool sqrRMEdone;
};

}


#endif /* MATRIXBUILDER_H_ */
