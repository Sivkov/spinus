/*
 * FinitDiffSolver.h
 *
 *  Created on: Jun 29, 2015
 *      Author: isivkov
 */

#ifndef FINITEDIFFSOLVER_H_
#define FINITEDIFFSOLVER_H_

#include <mkl.h>
#include "SDSolver.h"

namespace sps
{

template<class DataType>
class FiniteDiffSolver: public SDSolver<DataType>
{
public:
	FiniteDiffSolver(double timeStep, double maxTime):
		timeStep(timeStep), maxTime(maxTime), userFuncCallFrec(1) {}

	virtual ~FiniteDiffSolver(){};

	virtual void Solve();

	void UserFuncCallFrec(int passSteps);

protected:
	int userFuncCallFrec;
	double timeStep;
	double maxTime;
	vector<DataType> newSlvVec;
};


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template<class DataType>
void FiniteDiffSolver<DataType>::Solve()
{
	int n = maxTime/timeStep;

	vector<DataType> *rhs=nullptr;
	vector<DataType> *svec = SDSolver<DataType>::slvvec;

	if(svec==nullptr)
		return;

	newSlvVec.resize(svec->size());

	double time=0.0;

	for(int i=0; i<n ; i++)
	{
		time+=timeStep;

		SDSolver<DataType>::rhsFunc(&time, svec, rhs);



		if(i % userFuncCallFrec == 0)
		{
			this->userFunc();
		}
	}
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template<class DataType>
void FiniteDiffSolver<DataType>::UserFuncCallFrec(int passSteps)
{
	userFuncCallFrec = passSteps;
}

}


#endif /* FINITEDIFFSOLVER_H_ */
