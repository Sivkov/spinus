/*
 * SpinSinTypes.h
 *
 *  Created on: May 8, 2015
 *      Author: isivkov
 */

#ifndef SPINSINTYPES_H_
#define SPINSINTYPES_H_


#include <complex>

#undef MKL_Complex16
#define MKL_Complex16 std::complex<double>


#include "VectorUnsafe.h"
#include "MatrixUnsafe.h"

#include "Eigen/Dense"
#include "Eigen/SparseCore"
#include <mkl.h>

#include <tbb/tbb.h>
#include <tbb/concurrent_vector.h>

namespace sps
{

typedef unsafe::Matrix1D<MKL_Complex16> MatrixC;

typedef Eigen::SparseMatrix<double,Eigen::RowMajor> SparseMatrixD;
typedef Eigen::SparseMatrix<MKL_Complex16,Eigen::RowMajor> SparseMatrixZ;
typedef Eigen::Matrix<MKL_Complex16, Eigen::Dynamic, Eigen::Dynamic,Eigen::RowMajor> MatrixZ;
typedef Eigen::Matrix<MKL_Complex16, Eigen::Dynamic, 1> VectorZ;
typedef Eigen::Matrix<MKL_Complex16, 1, Eigen::Dynamic> VectorRowZ;
typedef Eigen::Triplet<MKL_Complex16> TripletZ;
typedef Eigen::Triplet<double> TripletD;
typedef tbb::concurrent_vector<TripletZ> TripletVectorZ;
typedef tbb::concurrent_vector<TripletD> TripletVectorD;

const MKL_Complex16 Im = MKL_Complex16(0.0,1.0);

const double PlanckConstPS		=	0.6582;
const double PlanckConstInvPS	=	1./0.6582;

//-------- just EIGEN's Triplet is sometimes shit, we made own ----------------------
template<class type1, class type2, class type3>
struct triplet
{
	type1 val1;
	type2 val2;
	type3 val3;
};
typedef triplet<int,int,SparseMatrixZ*> triplet_IIpSM;
typedef tbb::concurrent_vector<triplet_IIpSM> cvector_triplet_IIpSM;

}


const double Log2 = std::log(2.0);

#endif /* SPINSINTYPES_H_ */
