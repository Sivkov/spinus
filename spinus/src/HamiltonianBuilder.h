/*
 * OperatorFabrique.h
 *
 *  Created on: May 8, 2015
 *      Author: isivkov
 */

#ifndef HAMILTONIANBUILDER_H_
#define HAMILTONIANBUILDER_H_

#include <vector>

#include "SpinSinTypes.h"
#include "Tensor.h"
#include "JJState.h"
#include "QOperator.h"
#include "VarParameter.h"
#include "MatrixSparse.h"
#include "GlobalConfig.h"
#include "DensityMatrixSingle.h"
#include "DensityMatrixDouble.h"

namespace sps
{

//class VarParameter;
//class QOperator;

class HamiltonianBuilder
{
public:
	HamiltonianBuilder(JJState *jjState): jjState(jjState),configured(false),globalCfg(nullptr) { }

	~HamiltonianBuilder();

	void SetConfig(GlobalConfig *config) {globalCfg = config;}

	// add tensor to tensorStore
	void AddTensor(Tensor *tensor);
	void AddTensor(std::vector<Tensor*> tensors);

	// add varying parameter to varParamStore
	void AddVarParameter(VarParameter *varParam);
	void AddVarParameter(vector<VarParameter*> varParams);

	// add operator to qOpStore
	void AddQOperator(QOperator *qOp);

	void Configure();

	void ConfigureHam();

	void ConfigureObserv();

	void ConfigureDM();

	vector<VarParameter*>* GetVarParams();

	vector<MatrixSparse<MKL_Complex16>*>* GetObservMatrs(){return &observMatrs;}
	const vector<string>& GetObservNames(){ return observNames;}

	vector<MatrixSparse<MKL_Complex16>*>* GetSpinMatrs(){return &spinMatrs;}

	vector<DensityMatrixSingle*>* GetSingleDMs(){ return &singleDMs; }
	vector<DensityMatrixDouble*>* GetDoubleDMs(){ return &doubleDMs; }

	//void SolveSpinDynamics();

	//void SolveSpectrum();

	//void FiniteDiffSD();

	bool GetConfigured();

private:

	DensityMatrixSingle *getSingleDM(int spinInd);

	// save jjState , basis
	JJState *jjState;

	// all tensors will be here, this array is deallocatable
	vector<Tensor*> tensorStore;

	// varying parameters of hamiltonian
	vector<VarParameter*> varParamStore;

	// q operator store
	vector<QOperator*> qOpStore;

	// store to sort and effectively calculate matrices
	vector<pair<Tensor*, VarParameter*>> tensorToParams;

	vector<MatrixSparse<MKL_Complex16>*> observMatrs;
	vector<string> observNames;

	vector<MatrixSparse<MKL_Complex16>*> spinMatrs;

	vector<DensityMatrixSingle*> singleDMs;
	vector<DensityMatrixDouble*> doubleDMs;

	GlobalConfig *globalCfg;


	bool configured;
};

}

#endif
