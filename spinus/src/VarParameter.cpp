/*
 * VarParameter.cpp
 *
 *  Created on: Jun 4, 2015
 *      Author: isivkov
 */

#include <algorithm>
#include "VarParameter.h"

namespace sps
{



VarParameter::~VarParameter()
{

}

const MatrixC&  VarParameter::SetupMatrix(long int dimension)
{
	pTensorMatrix.Clear();

	pTensorMatrix = MatrixC(dimension, dimension,0);

	return pTensorMatrix;
}


void VarParameter::AddTensor(Tensor *tensor)
{
	varTensors.push_back(tensor);
}


void VarParameter::AddTensor(const std::vector<Tensor*> &tensors)
{
	for(auto pt : tensors )
	{
		if(pt!=nullptr) varTensors.push_back(pt);
	}
}


void VarParameter::AddTensorsFromQOperator(QOperator *qOp)
{
	AddTensor(qOp->GetAllTensors());
}


void VarParameter::AddTensorsFromQOperator(const std::vector<QOperator*> &qOps)
{
	for(auto qop: qOps)
	{
		AddTensorsFromQOperator(qop);
	}
}

void VarParameter::RemoveTensors(const std::vector<Tensor*> &tensors)
{
	for(auto pt: tensors)
	{
		varTensors.erase( std::remove( varTensors.begin(), varTensors.end(), pt ), varTensors.end() );
	}
}


const std::vector<Tensor*> VarParameter::GetTensors() const
{
	return varTensors;
}




}
