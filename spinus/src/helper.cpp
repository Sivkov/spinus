/*
 * helper.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: isivkov
 */



#include "helper.h"

namespace sps
{

std::string ToLower(std::string str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);

	return str;
}

}
