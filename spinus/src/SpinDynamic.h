/*
 * SpinDynamic.h
 *
 *  Created on: Jul 20, 2015
 *      Author: isivkov
 */

#ifndef SPINDYNAMIC_H_
#define SPINDYNAMIC_H_

#include <utility>
#include <vector>
#include <string>
#include <fstream>
#include <omp.h>

#include "SpinSinTypes.h"
#include <mkl.h>

#include "JJState.h"
#include "Tensor.h"
#include "HamiltonianBuilder.h"
#include "SingleTensor.h"
#include "PairTensor.h"

#include "ZeemanQOp.h"
#include "HeisenbergQOp.h"
#include "IonAnisQOp.h"
#include "SpinQOp.h"

#include "MatrixBuilder.h"
#include "VarParameter.h"
#include "MatrixSparse.h"

#include "SpinDynamicConfig.h"

using namespace std;


namespace sps
{


class SpinDynamic
{
public:
	SpinDynamic();
	virtual ~SpinDynamic();

	void SetJJState(JJState *jjs);
	void SetHamBuilder(HamiltonianBuilder *hb);

	void SetConfig(SpinDynamicConfig &config);

	void Calc();
	//sparse_status_t dd;
protected:
	virtual void userInit();
	virtual void userCalc();

	void paramInit();
	void baseInit();
	void initDM();

	//void outputEigVal();
	void outputMagMom();
	void outputObserv();
	void outputParams();
	void outputDM();

	void calcParamsForStep(int step);
	void calcParamsForTime(double time);

	void calcTotalMatrix();
	void calcTotalMatrixForTime(double time);
	void eigSolve();

	void calcMagMom(int saveInd);
	void saveEnergy(int saveInd);
	void calcObservables(int saveInd);
	void calcDM(int saveInd);

	void calcRHS(MKL_Complex16 *eigvecdata, MKL_Complex16 *rhsdata, MKL_Complex16 *tmpdata, long int size);
	void calcRHS();
	double calcEnergy(MKL_Complex16 *eigv, MKL_Complex16 *tmp);
	bool calcStep();
	void RungeKuttaStep();

	//-----------------------------------------
	int parLoopIndex;
	int maxParDataSize;
	long basisDim;

	// step numer during dynamics
	int stepsNum;

	// external
	HamiltonianBuilder *hamBuilder;
	JJState *jjState;
	vector<VarParameter*> *params;


	// own config
	SpinDynamicConfig config;
	int numOutSpins;
	double damping;
	double timeStep;
	MKL_Complex16 dampCoef1, dampCoef2, imPlanckCoef;
	int numSaveSteps;
	int saveEverySteps;

	// array for all iterations
	vector<double> observables;
	vector<double> magMoms;
	vector<double> energies;
	vector<double> times;
	vector<double> entropy;

	// rhs of the dynamics diff eq
	vector<MKL_Complex16> rhs;

	// Runge-Kutta vectors
	vector<MKL_Complex16> k1,k2,k3,k4;
	vector<MKL_Complex16> tmp1,tmp2,tmp3,tmp4;

	// variables for current parameter loop interation
	double energy;
	double time;
	int saveInd;

	// vectors used for current iteration
	vector<double> curMagMoms;
	vector<MKL_Complex16> curParams;
	vector<double> curEigVals;
	vector<MKL_Complex16> curEigVecs;
	vector<vector<MKL_Complex16>> curDMs;
	vector<vector<MKL_Complex16>> curDMeigvals;


	// eigvec of user defined vector (probably)
	vector<MKL_Complex16> startEigVec;

	vector<MatrixSparse<MKL_Complex16>*> spinMatrs;
	vector<DMAbstract*> densityMatrices;

	// to account temperature distribution
	vector<double> Zfactors;
	double Zsum;

	// store parameterized parts of Hamiltonian for quick access
	vector<MKL_Complex16*> parMatDatas;

	// total matrix
	MatrixC totmat;


	// MKL setups
	int numeigfnd;
	vector<int> isuppz; 				// = new int[2 * numeig];
	vector<MKL_Complex16> tmpvec2;		// = new MKL_Complex16[dim];
	int numThreads;

	bool breakCalc;
	bool calcObserv;
	bool configured;
};

}


#endif /* SPINDYNAMIC_H_ */
