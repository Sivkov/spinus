/*
 * OperatorFabrique.cpp
 *
 *  Created on: Jun 3, 2015
 *      Author: isivkov
 */


#include <algorithm>

#include "HamiltonianBuilder.h"
#include "MatrixBuilder.h"
//#include "FiniteDiffSolver.h"

namespace sps
{
HamiltonianBuilder::~HamiltonianBuilder()
{
	for(auto dms: singleDMs)
	{
		delete dms;
	}

	for(auto dmd: doubleDMs)
	{
		delete dmd;
	}
}

//--------------------------------------------------------------------------------
bool TensorToParamsCompare(const pair<Tensor*, VarParameter*> &ttp1, const pair<Tensor*, VarParameter*> &ttp2)
{
	return ( (*ttp1.first) < (*ttp2.first));
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddTensor(Tensor *tensor)
{
	tensorStore.push_back(tensor);
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddTensor(std::vector<Tensor*> tensors)
{
	tensorStore.insert(tensorStore.end(),tensors.begin(), tensors.end());
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddVarParameter(vector<VarParameter*> varParams)
{
	varParamStore.insert(varParamStore.end(),varParams.begin(),varParams.end());
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddVarParameter(VarParameter *varParam)
{
	varParamStore.push_back(varParam);
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::AddQOperator(QOperator *qOp)
{
	qOpStore.push_back(qOp);
}


//--------------------------------------------------------------------------------
vector<VarParameter*>* HamiltonianBuilder::GetVarParams()
{
	return &varParamStore;
}




//--------------------------------------------------------------------------------
bool HamiltonianBuilder::GetConfigured()
{
	return configured;
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::Configure()
{
	ConfigureHam();
	ConfigureObserv();
	ConfigureDM();
}

void HamiltonianBuilder::ConfigureHam()
{
	// add all tensors and varParams to  vector of pairs
	for(auto param: globalCfg->parameters)
	{
		for(auto tensor: param->GetTensors())
		{
			if(tensor != nullptr)
				tensorToParams.push_back(pair<Tensor*, VarParameter*>(tensor, param));
		}
	}

	// sort by tensors
	std::sort(tensorToParams.begin(), tensorToParams.end(), TensorToParamsCompare);

	//--------------------------------------------------------------
	// Calculate matrices and add to varMatrices or smth like that
	//--------------------------------------------------------------

	// init var param matrices and add them to parameters
	long int basisDim = jjState->GetBasisDimension();

	// setup matrices to use them after
	for(auto param: globalCfg->parameters)
	{
		param->SetupMatrix(basisDim);
	}

	// setup MatrixBuilder to calculate matrices from tensors
	MatrixBuilder *mb = new MatrixBuilder();
	mb->SetJJState(jjState);

	for(auto ttp : tensorToParams)
	{
		MatrixC *pTensM = ttp.second->GetMatrixUnsafe();
		mb->BuildFromTensor(*ttp.first, pTensM);
	}

	delete mb;

	//TODO undo shitcode
	varParamStore = globalCfg->parameters;

	this->configured=true;
}

//--------------------------------------------------------------------------------
void HamiltonianBuilder::ConfigureObserv()
{
	long int basisDim = jjState->GetBasisDimension();

	MatrixC *mat = new MatrixC(basisDim,basisDim,0);

	MatrixBuilder *matBldr = new MatrixBuilder();
	matBldr->SetJJState(jjState);

	int ind=0;

	observMatrs.resize(globalCfg->observables.size());

	//TODO make all in sparse view without building dense matrix
	for(int i=0;i< globalCfg->observables.size() ; i++)
	{
		observNames.push_back(globalCfg->observables[i]->GetName());

		mat->ZeroMemory();

		vector<Tensor*> tensors = globalCfg->observables[i]->GetTensors();

		matBldr->BuildFromTensor(tensors,mat);

		observMatrs[i] = new MatrixSparse<MKL_Complex16>();

		observMatrs[i]->COOFromDenseTr(mat->GetData(),mat->GetWidth());

		ind++;
	}

	delete mat;
	delete matBldr;
}

//--------------------------------------------------------------------------------
DensityMatrixSingle *HamiltonianBuilder::getSingleDM(int spinInd)
{
	auto it = std::find_if(singleDMs.begin(),singleDMs.end(),
			[spinInd](DensityMatrixSingle *dm)->bool
			{
				if (dm->GetSpinIndices()[0]==spinInd)
				{
					return true;
				}

				return false;
			});

	if ( it==singleDMs.end())
	{
		DensityMatrixSingle *dm= new DensityMatrixSingle(jjState,spinInd);
		dm->BuildMatrix();
		return dm;
	}
	else
	{
		return *it;
	}
}


//--------------------------------------------------------------------------------
void HamiltonianBuilder::ConfigureDM()
{
	for(auto i: globalCfg->singleDMInds)
	{
		DensityMatrixSingle* dm = getSingleDM(i);

		singleDMs.push_back(dm);
	}

	for(auto p: globalCfg->doubleDMInds)
	{
		DensityMatrixSingle* dm1 = getSingleDM(p.first);
		DensityMatrixSingle* dm2 = getSingleDM(p.second);

		DensityMatrixDouble *dmd = new DensityMatrixDouble(dm1,dm2);

		doubleDMs.push_back(dmd);
	}
}
}
