/*
 * SpinDynamics.cpp
 *
 *  Created on: Jul 21, 2015
 *      Author: isivkov
 */



/*
 * SpinStatic.cpp
 *
 *  Created on: Jul 10, 2015
 *      Author: isivkov
 */

#include <iostream>
#include <iomanip>


#include "SpinDynamic.h"
#include "MatrixSparse.h"
#include "MathHelper.h"

#include <mkl_spblas.h>

using namespace std;

namespace sps
{

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
SpinDynamic::SpinDynamic():
		hamBuilder(nullptr),
		jjState(nullptr),
		numeigfnd(0),
		basisDim(0),
		configured(false),
		parLoopIndex(0),
		Zsum(0),
		numOutSpins(0),
		maxParDataSize(0),
		numThreads(1),
		damping(0.0),
		energy(0.0),
		timeStep(0.001),
		params(nullptr),
		numSaveSteps(0),
		calcObserv(false),
		breakCalc(false)
{ }


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
SpinDynamic::~SpinDynamic()
{
	for(auto sm: spinMatrs)
	{
		delete sm;
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::SetJJState(JJState *jjs)
{
	jjState=jjs;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::SetHamBuilder(HamiltonianBuilder *hb)
{
	hamBuilder=hb;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::SetConfig(SpinDynamicConfig &config)
{
	this->config=config;
	configured=true;
	numOutSpins = config.spinNumsForMagMom.size();
	damping = config.damping;
	timeStep = config.timeStep;
	stepsNum = config.timeEnd / timeStep;
	saveEverySteps = config.saveEveryPS / timeStep  ;
	if(saveEverySteps==0)	saveEverySteps = 1 ;
	numSaveSteps = stepsNum / saveEverySteps;
}




//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::Calc()
{
	if( !(hamBuilder->GetConfigured()) )
	{
		throw SpinException("Hamiltonian is not configured");
	}

	//-----------------------------------------------------------------
	// prepare
	//-----------------------------------------------------------------

	// param init, for quicker access
	paramInit();

	// base inits, allocation some arrays and others
	baseInit();

	initDM();


	cout<<"-----------------------------------"<<endl;

	cout<<"number of steps:\t"<< stepsNum<<endl;
	cout<<"save data every:\t"<< saveEverySteps<<" steps"<<endl;
	cout<<"num save steps:\t" << numSaveSteps<<endl;

	cout<<"-----------------------------------"<<endl;

	//find needed eigvals and/or vectors
	cout<<"Find eigenvals for first step"<<endl;

	saveInd=0;	
	calcTotalMatrixForTime(0.0);
	/*
	cout<<"H"<<endl;
		for(int i = 0; i< basisDim; i++)
		{
			for(int j=0; j<basisDim; j++)
			{
				cout<<totmat.GetData()[i*basisDim+j]<<" ";
			}
			cout<<endl;
		}
		cout<<endl;
		*/
	eigSolve();

	// stupidity
	calcTotalMatrixForTime(0.0);
	
	/*
		cout<<"H"<<endl;
		for(int i = 0; i< basisDim; i++)
		{
			for(int j=0; j<basisDim; j++)
			{
				cout<<totmat.GetData()[i*basisDim+j]<<" ";
			}
			cout<<endl;
		}
		cout<<endl;
	*/
	
	MKL_Complex16 *dat = totmat.GetData();

	energy = curEigVals[0];
	saveEnergy(0);
	calcMagMom(0);
	calcDM(0);

	cout<<"Start main loop"<<endl;

	// counter for output during calc
	int outEvery = stepsNum/100 + 1;
	int curCount=0;
	int percentDone=0;

	int saveTime=0;

	time=0.0;


	//-----------------------------------------------------------------
	// main loop
	//-----------------------------------------------------------------

	for(int k=0;k<stepsNum-1; k++)
	{
		parLoopIndex = k;
	/*
////////////////////// DEBUG //////////////////////////////////////////////////
	cout<<"Params"<<endl;
		for(int l=0; l<params->size(); l++)
		{
			const vector<double>& pardata = (*params)[l]->GetParData();	
			if(pardata.size()==0)cout<<"1.0 "<<endl;
			else
			cout<<pardata[k<pardata.size() ? k : pardata.size()-1]<<" "<<endl;
		
			for(int i = 0; i< basisDim; i++)
			{
				for(int j=0; j<basisDim; j++)
				{
					cout<<parMatDatas[l][i*basisDim+j]<<" ";
				}
				cout<<endl;
			}
		}
		cout<<endl;
		cout<<"H"<<endl;
		for(int i = 0; i< basisDim; i++)
		{
			for(int j=0; j<basisDim; j++)
			{
				cout<<totmat.GetData()[i*basisDim+j]<<" ";
			}
			cout<<endl;
		}
		cout<<endl;

		cout<<"WF"<<endl;
		for(int i = 0; i< basisDim; i++)
		{
			cout<<std::abs(curEigVecs[i])<<" ";
		}
		cout<<endl;
		
		cin.get();
///////////////////////////////////////////////////////////////////////////////
	*/
		if(!calcStep())
                        break;

		//save physical data
		if(saveTime == saveEverySteps && saveInd < numSaveSteps -1)
		{
			saveInd++;
			calcMagMom(saveInd);
			calcObservables(saveInd);
			saveEnergy(saveInd);
			calcDM(saveInd);

			saveTime=0;
		}
		saveTime++;

		// output amount of performed work
		curCount++;
		if(curCount == outEvery)
		{
			//normalize vector
			double norm = cblas_dznrm2 (basisDim, curEigVecs.data(), 1);
			percentDone++;
			cout << percentDone<<"% done. wave function norm = "<<norm<<endl;
			curCount=0;
		}

		time+=timeStep;
	}

	if(!breakCalc)
	{
		cout<<"Calculation finished, output results"<<endl;

		outputMagMom();
		outputObserv();
		outputParams();
		outputDM();
	}

}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::saveEnergy(int saveInd)
{
	energies[saveInd] = energy;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::userInit()
{

}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::userCalc()
{

}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::baseInit()
{
	//------------------------
	// base init
	//------------------------
	basisDim = jjState->GetBasisDimension();

	curEigVals.resize(basisDim);
	curEigVecs.resize(basisDim);

	// for Runge-Kutta
	rhs.resize(basisDim);
	k1.resize(basisDim);
	k2.resize(basisDim);
	k3.resize(basisDim);
	k4.resize(basisDim);
	tmp1.resize(basisDim);
	tmp2.resize(basisDim);
	tmp3.resize(basisDim);
	tmp4.resize(basisDim);

	// to store total matrix after summation with parameters
	totmat=MatrixC(basisDim,basisDim,0);

	// constants for Schroedinger equation
	dampCoef1 = ( - Im - damping) / ( PlanckConstPS * (1+ damping * damping));
	dampCoef2 = damping / (PlanckConstPS * (1+ damping * damping));
	imPlanckCoef = -Im/PlanckConstPS;

	//---------------------
	// magmom init
	//---------------------
	int n = jjState->getInitTwoJ().size();

	//init magmom arrays
	tmpvec2.resize(basisDim*numOutSpins*3);
	isuppz.resize(2);
	curMagMoms.resize(numOutSpins*3);
	spinMatrs.resize(numOutSpins*3);
	magMoms.resize(numSaveSteps*numOutSpins*3);
	energies.resize(numSaveSteps);

	//TODO move this to Hamiltonian Builder
	// create spin matrices
	MatrixC *spinmat = new MatrixC(basisDim,basisDim,0);

	MatrixBuilder *spinMatBldr = new MatrixBuilder();
	spinMatBldr->SetJJState(jjState);

	int ind=0;

	for(int i=0;i<numOutSpins; i++)
	{
		SpinQOp sop(config.spinNumsForMagMom[i],n);

		for(int j=0; j<3; j++)
		{
			spinmat->ZeroMemory();

			spinMatBldr->BuildFromTensor(sop.GetComponents(j),spinmat);

			spinMatrs[ind] = new MatrixSparse<MKL_Complex16>();

			spinMatrs[ind]->COOFromDenseTr(spinmat->GetData(),spinmat->GetWidth());

			ind++;
		}
	}

	delete spinmat;
	delete spinMatBldr;

	//-----------------------------------------------
	// observables
	//-----------------------------------------------
	if(hamBuilder->GetObservMatrs()->size()!=0)
	{
		observables.resize(hamBuilder->GetObservMatrs()->size()*numSaveSteps);
		calcObserv=true;
	}

	//-----------------------------------------------
	// Density Matrices
	//-----------------------------------------------

}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::paramInit()
{
	//TODO make good parameter system. This is shit
	// get params for loop
	params = hamBuilder->GetVarParams();

	// save to local variable because I dont want to make a mess with this *&^*%^
	vector<VarParameter*> &par = *params;

	// basic arrays init
	curParams.resize(params->size());
	//curParams[0]=1.0;

	//get params and store max pardata length
	// for quicker access
	//if(params->size()>1)
	//{
	//	maxParDataSize=par[1]->GetParData().size();
	//}

	// add first unparameterized matrix
	//MKL_Complex16 *data=par[0]->GetMatrixUnsafe()->GetData();
	//parMatDatas.push_back(data);

	// loop for params
	for(int i = 0; i< par.size(); i++)
	{
		const vector<double>& pardata = par[i]->GetParData();

		if(maxParDataSize < pardata.size()) maxParDataSize = pardata.size();
		//if(pardata.size() != maxParDataSize)
		//{
		//	throw SpinException("Error: parameter data for different parameters has different length");
		//}

		// store matrix data
		MKL_Complex16 *data=par[i]->GetMatrixUnsafe()->GetData();
		parMatDatas.push_back(data);

		if( par[i]->GetArgStep() == 0)
		{
			par[i]->SetArgStep(timeStep);
		}
	}

}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::initDM()
{
	vector<DensityMatrixSingle*> &dmsv = *hamBuilder->GetSingleDMs();
	vector<DensityMatrixDouble*> &dmdv = *hamBuilder->GetDoubleDMs();

	// copy all matrices pointers in one array for easier access
	densityMatrices.insert(densityMatrices.end(),dmsv.begin(), dmsv.end());
	densityMatrices.insert(densityMatrices.end(),dmdv.begin(), dmdv.end());

	entropy.resize(densityMatrices.size()*numSaveSteps,0.0);

	curDMeigvals.resize(densityMatrices.size());
	curDMs.resize(densityMatrices.size());

	for(int i = 0; i< densityMatrices.size(); i++)
	{
		curDMs[i].resize(densityMatrices[i]->GetDimensions() * densityMatrices[i]->GetDimensions(),0.0);
		curDMeigvals[i].resize(densityMatrices[i]->GetDimensions(),0.0);
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputMagMom()
{
	// magmom file reopen and write head line
	ofstream magf(config.magMomOutFile,std::ofstream::out);

	if(magf.is_open())
	{
		magf<<"Time\t";
		magf<<"Energy\t";

		for(int i=0;i<numOutSpins; i++)
		{
			magf <<"S"<<i<<"_"<<"x"<<"\t";
			magf <<"S"<<i<<"_"<<"y"<<"\t";
			magf <<"S"<<i<<"_"<<"z"<<"\t";
		}

		magf<<endl;

		for(int j=0; j<numSaveSteps; j++)
		{
			magf << j*config.saveEveryPS << "\t";
			magf << energies[j] << "\t";

			for(int i=0;i<numOutSpins*3; i++)
			{
				magf <<std::fixed<<std::setprecision(6)<<magMoms[j*numOutSpins*3+i]<<"\t";
			}
		magf<<endl;
		}

		magf.close();
	}
	else
	{
		cout<<"file is not opened"<<endl;
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputObserv()
{
	vector<MatrixSparse<MKL_Complex16>*>& observMatrs = *(hamBuilder->GetObservMatrs());

	// magmom file reopen and write head line
	ofstream magf(config.observablesFile,std::ofstream::out);

	if(magf.is_open())
	{
		// write first line
		magf<<"Time\t";

		for(auto n: hamBuilder->GetObservNames())
		{
			magf<<n<<"\t";
		}

		magf<<endl;

		for(int j=0; j<numSaveSteps; j++)
		{
			magf << j*config.saveEveryPS << "\t";

			for(int i=0;i<observMatrs.size(); i++)
			{
				magf <<std::fixed<<std::setprecision(6)<<observables[j*observMatrs.size()+i]<<"\t";
			}
		magf<<endl;
		}

		magf.close();
	}
	else
	{
		cout<<"file is not opened"<<endl;
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputDM()
{
	// magmom file reopen and write head line
	ofstream magf(config.entropyFile,std::ofstream::out);
	if(magf.is_open())
	{
		// write first line
		magf<<"Time\t";

		for(auto m: densityMatrices)
		{
			magf<<"E_";
			for(auto i: m->GetSpinIndices()) magf<<i<<"_";
		}

		magf<<endl;

		for(int j=0; j<numSaveSteps; j++)
		{
			magf << j*config.saveEveryPS << "\t";

			for(int i=0;i<densityMatrices.size(); i++)
			{
				magf <<std::fixed<<std::setprecision(6)<<entropy[j*densityMatrices.size()+i]<<"\t";
			}
		magf<<endl;
		}

		magf.close();
	}
	else
	{
		cout<<"file is not opened"<<endl;
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::outputParams()
{
	// files reopen and write head line
	vector<VarParameter*> &pars = *params;

	for(auto par: pars)
	{
		if(par->GetState()==VarParameter::Fixed)
			continue;

		string pname = par->GetName();

		ofstream magf("_param_"+pname+".dat",std::ofstream::out);

		if(magf.is_open())
		{
			// write first line
			magf<<"Time\t"<<pname<<endl;

			for(int j=0; j<par->GetParData().size(); j++)
			{
				magf << j*par->GetArgStep() << "\t" << par->GetParData()[j] << endl;
			}

			magf.close();
		}
		else
		{
			cout<<"file is not opened"<<endl;
		}
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcParamsForStep(int step)
{
	if( step > maxParDataSize) return;

	// TODO optimization: all params in one array
	// setup parameters for this iteration
	for(int i = 0; i< params->size(); i++)
	{
		const vector<double>& pardata = (*params)[i]->GetParData();

		int psize = pardata.size();

		if(psize > step)
		{
			curParams[i] = pardata[step];
		}


		if(psize==0 || (*params)[i]->GetState() == VarParameter::Fixed)
		{
			curParams[i]=1.0;
		}
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcParamsForTime(double time)
{


	// TODO optimization: all params in one array
	// setup parameters for this iteration
	for(int i = 0; i< params->size(); i++)
	{
		double parstep = (*params)[i]->GetArgStep();
		int step = time / parstep;

		const vector<double>& pardata = (*params)[i]->GetParData();

		int psize = pardata.size();

		// if check if we have no parameter array for current parameter
		if(psize==0 || (*params)[i]->GetState() == VarParameter::Fixed)
		{
			curParams[i]=1.0;
			continue;
		}

		// else if we have array of parameters
		// calc current step
		double fractpart = time - step*parstep;

		// if we reached end of parameter array
		if(psize > step-1)
		{
			// interpolation
			curParams[i] = pardata[step] + (pardata[step+1]-pardata[step]) * fractpart/parstep;
		}
		else
		{
			curParams[i] = pardata[psize-1];
		}
	}
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcTotalMatrix()
{
	//if( parLoopIndex > maxParDataSize) return;

	MKL_Complex16 *totmdat = totmat.GetData();

	int parsize = curParams.size();

	long int msize = totmat.GetSize();

	// TODO: all matrices in one array
#pragma omp parallel for //simd firstprivate(par,par2)
	for(long int i=0; i<msize; i++)
	{
		//totmdat[i]=0.0;
		MKL_Complex16 res = 0.0;

//#pragma simd  vectorlengthfor(MKL_Complex16) reduction(+:res)
		for(int j=0; j < parsize; j++ )
		{	//cout<<curParams[j]<<" * "<<parMatDatas[j][i]<<" + ";
			res += curParams[j] * parMatDatas[j][i];
		}//cout<<" = "<<res<<endl;

		totmdat[i]=res;
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcTotalMatrixForTime(double time)
{
	int step = time/timeStep;
	
	if(step > maxParDataSize) return;
	
	calcParamsForTime(time);
	calcTotalMatrix();
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
double SpinDynamic::calcEnergy(MKL_Complex16 *eigv, MKL_Complex16 *tmp)
{
	MKL_Complex16 alpha=1.0,beta=0.0;
	MKL_Complex16 res;

	cblas_zhemv(CBLAS_ORDER::CblasRowMajor, CBLAS_UPLO::CblasLower, basisDim, &alpha,
					totmat.GetData(), basisDim, eigv, 1, &beta, tmp, 1);

	cblas_zdotc_sub(basisDim, eigv, 1, tmp, 1, &res);

	energy = res.real();
	return energy;
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcRHS(MKL_Complex16 *eigvecdata, MKL_Complex16 *rhsdata, MKL_Complex16 *tmpdata, long int size)
{
	MKL_Complex16 res;
	MKL_Complex16 alpha=1.0,beta=0.0;

	MKL_Complex16 *totmdat = totmat.GetData();

	if(damping != 0)
	{
		energy = calcEnergy(eigvecdata, tmpdata);

		// copy eigvec to rhs, because rhs will be overwritten
		memcpy(rhsdata,eigvecdata, sizeof(MKL_Complex16) * size);

		beta = energy * dampCoef2;

		// calc rhs
		cblas_zhemv(CBLAS_ORDER::CblasRowMajor, CBLAS_UPLO::CblasLower, size, &dampCoef1,
				totmdat, size, eigvecdata, 1, &beta, rhsdata, 1);
	}
	else
	{
		cblas_zhemv(CBLAS_ORDER::CblasRowMajor, CBLAS_UPLO::CblasLower, size, &imPlanckCoef,
				totmdat, size, eigvecdata, 1, &beta, rhsdata, 1);
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcRHS()
{
	calcRHS(curEigVecs.data(), rhs.data(), tmp1.data(), basisDim);
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
bool SpinDynamic::calcStep()
{
	//TODO adaptive step
	RungeKuttaStep();

	double norm = cblas_dznrm2 (basisDim, curEigVecs.data(), 1);

        MKL_Complex16 invnorm = 1./norm;
        cblas_zscal(basisDim,&invnorm,curEigVecs.data(),1);
/*
	if(std::abs(norm-1) > 0.0001)
	{
		cout<<"WARNING! Norm of wave function is not 1: "<<norm<<"! Decrease step. Breaking program..."<<endl;
		breakCalc = true;
		return false;
	}

	if(std::abs(norm-1) > 0.00001)
	{
		MKL_Complex16 invnorm = 1./norm;
		cblas_zscal(basisDim,&invnorm,curEigVecs.data(),1);
	}
*/
	return true;
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::RungeKuttaStep()
{
	MKL_Complex16 *eigv=curEigVecs.data();
	MKL_Complex16 *k1dat = k1.data();
	MKL_Complex16 *k2dat = k2.data();
	MKL_Complex16 *k3dat = k3.data();
	MKL_Complex16 *k4dat = k4.data();

	// k1
	calcRHS(eigv, k1dat, tmp1.data(), basisDim);

	// k2
	calcTotalMatrixForTime(time + timeStep/2.);
	sps_zaxpy(basisDim, timeStep/2., k1dat , eigv , tmp1.data() );
	calcRHS(tmp1.data(), k2dat, tmp2.data(), basisDim);

	// k3
	sps_zaxpy(basisDim, timeStep/2., k2dat, eigv, tmp1.data() );
	calcRHS(tmp1.data(), k3dat, tmp2.data(), basisDim);

	// k4
	calcTotalMatrixForTime(time + timeStep);
	sps_zaxpy(basisDim, timeStep, k3dat, eigv , tmp1.data() );
	calcRHS(tmp1.data(), k4dat, tmp2.data(), basisDim);

	MKL_Complex16 stepc=timeStep/6.;
	MKL_Complex16 cc(2.,0.);

#pragma omp parallel for
	for(int i=0; i<basisDim; i++)
	{
		eigv[i] = eigv[i] + (k1[i] + cc * k2[i] + cc * k3[i] + k4[i]) * stepc ;
	}

}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::eigSolve()
{
	MKL_Complex16 *totmdat = totmat.GetData();
	double *ev = curEigVals.data();

	int cr = LAPACKE_zheevr( LAPACK_ROW_MAJOR, 'V', 'I', 'L',
			basisDim, totmdat, basisDim, 0.0, 1.0, config.initEigState+1, config.initEigState+1, 0.0,
			&numeigfnd, ev, curEigVecs.data(), config.initEigState+1, isuppz.data() );
			
		if(cr!=0) throw SpinException("Error in eigenvalues calculation");	

}






//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcMagMom(int saveInd)
{
	MKL_Complex16 res=0.0;
	MKL_Complex16 alpha=1.0,beta=0.0;

	omp_set_nested(0);

#pragma omp parallel for private(res)
	for(int i=0; i<numOutSpins*3; i++) // over spins
	{
		MKL_Complex16 *tmpptr = &(tmpvec2[basisDim*i]);

		MKL_Complex16 *eigvecdata = curEigVecs.data();

		int numel = spinMatrs[i]->GetNumElem();

		int dim = basisDim;

		MKL_Complex16 *val = spinMatrs[i]->GetValuesRaw();
		int *indI = spinMatrs[i]->GetMainIndsRaw();
		int *indJ = spinMatrs[i]->GetPBRaw();

		mkl_zcoomv("N", &dim, &dim, &alpha, "HLNCAA",
				val, indI, indJ,
				&numel, eigvecdata, &beta, tmpptr);

		cblas_zdotc_sub(basisDim, eigvecdata, 1, tmpptr, 1, &res);

		magMoms[saveInd*numOutSpins*3 + i] = res.real();

	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcObservables(int saveInd)
{
	if(!calcObserv) return;

	MKL_Complex16 res=0.0;
	MKL_Complex16 alpha=1.0,beta=0.0;

	omp_set_nested(0);

	vector<MatrixSparse<MKL_Complex16>*>& observMatrs = *(hamBuilder->GetObservMatrs());

#pragma omp parallel for private(res)
	for(int i=0; i < observMatrs.size(); i++) // over spins
	{
		MKL_Complex16 *tmpptr = &(tmpvec2[basisDim*i]);

		MKL_Complex16 *eigvecdata = curEigVecs.data();

		int numel = observMatrs[i]->GetNumElem();

		MKL_Complex16 *val = observMatrs[i]->GetValuesRaw();
		int *indI = observMatrs[i]->GetMainIndsRaw();
		int *indJ = observMatrs[i]->GetPBRaw();

		sps_zcoovmv(basisDim, eigvecdata, tmpptr, numel, val, indI, indJ, &res);

		/*
		mkl_zcoomv("N", &dim, &dim, &alpha, "HLNCAA",
				val, indI, indJ,
				&numel, eigvecdata, &beta, tmpptr);

		cblas_zdotc_sub(basisDim, eigvecdata, 1, tmpptr, 1, &res);
		*/

		observables[saveInd*observMatrs.size() + i] = res.real();
	}
}



//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
void SpinDynamic::calcDM(int saveInd)
{
	omp_set_nested(0);

	int size = densityMatrices.size();

#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		densityMatrices[i]->CalcDensityMatrix(curEigVecs.data(), basisDim, curDMs[i].data());

/*		cout<<"H"<<endl;
				for(int l = 0; l< densityMatrices[i]->GetDimensions(); l++)
				{
					for(int m=0; m<densityMatrices[i]->GetDimensions(); m++)
					{
						cout<<curDMs[i][l*densityMatrices[i]->GetDimensions()+m]<<" ";
					}
					cout<<endl;
				}
				cout<<endl;
*/
		entropy[saveInd*size + i] = calcVonNeumanEntropy(curDMs[i].data(),curDMeigvals[i].size(),curDMeigvals[i].data());
	}
}

}



