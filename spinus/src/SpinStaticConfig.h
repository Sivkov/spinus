/*
 * SpinStaticConfig.h
 *
 *  Created on: Jan 11, 2016
 *      Author: isivkov
 */

#ifndef SPINSTATICCONFIG_H_
#define SPINSTATICCONFIG_H_

#include <vector>

using namespace std;

namespace sps
{

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
struct SpinStaticConfig
{
	bool calcMagMom;
	bool writeAllMagMom;
	bool writeEigVals;
	bool calcEigVecs;
	int numEig;
	double temperature; //meV
	vector<vector<int>> spinNumsForMagMom;
	vector<int> spinComponents; // 0-x, 1-y, 2-z
	string eigValOutFile;
	string magMomOutFile;
	string magMomPerEigFile;

	bool isConfigured;
};

}

#endif /* SPINSTATICCONFIG_H_ */
