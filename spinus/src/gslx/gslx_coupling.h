/*
 * gslx_coupling.h
 *
 *  Created on: Jan 26, 2015
 *      Author: isivkov
 */

#ifndef GSLX_COUPLING_H_
#define GSLX_COUPLING_H_

#include <gsl/gsl_math.h>
#include <gsl/gsl_sf.h>

namespace gslx
{


double coupling_9j(int twoJconf[9]);


}
#endif /* GSLX_GSLX_COUPLING_H_ */
