/*
 * OperatorsParser.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: isivkov
 */

#include "OperatorParser.h"

namespace sps
{

RecordInfo OperatorParser::Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	if(config->numberOfSpins == 0 || config->spins.size()==0)
	{
		throw SpinException("HeisOpParser: error, spin array is not set ");
	}

	RecordInfo rinfo = findRecordByKeyWord(keyWord, scriptTokens, start, stop);

	if( rinfo.type == RecordType::Line )
	{
		return readLineRecord(config, rinfo);
	}

	if( rinfo.type == RecordType::Block )
	{
		return readBlockRecord(config, rinfo);
	}

	return rinfo;
}




VarParameter* OperatorParser::getVarParam(GlobalConfig *config, string parName, VarParameter::State parState)
{
	VarParameter* par=nullptr;

	vector<VarParameter*>::iterator it = find_if(config->parameters.begin(),config->parameters.end(),
			[parName](VarParameter* par){ return par->GetName() == parName; }
	);

	if(it == config->parameters.end())
	{
		// add new parameter to store
		AbstractStore<VarParameter> *store = StoreLocator<VarParameter>::GetStore();

		par = store->Add( new VarParameter(parName,parState) );
		config->parameters.push_back(par);
	}
	else
	{
		par = (*it);
	}

	if(par == nullptr)
	{
		throw SpinException("HeisOpParser: Something wrong with parameters of Hamiltonian");
	}

	return par;
}




}
