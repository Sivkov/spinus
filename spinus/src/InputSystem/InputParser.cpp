/*
 * InputParser.cpp
 *
 *  Created on: Sep 24, 2015
 *      Author: isivkov
 */

#include <fstream>
//#include <regex>
//#include <tr1/regex>
#include <iostream>
#include <sstream>

#include "InputParser.h"

using namespace std;


namespace sps
{

vector<string> InputParser::ReadScriptFile(string fileName)
{
	// read file to string and delete comments
	ifstream file(fileName);
	string str;
	string file_contents;

	while (std::getline(file, str))
	{
		// delete comments
		int commentPos = str.find_first_of(comment);

		if(commentPos==0) continue;

		string purestr = str.substr(0,commentPos);

		// take tokens with delimiters (if they are exist)
		stringstream ss;
		ss.str(purestr);
		string upt;

		while((ss>>upt))
		{
		// split each token by delimiters  (if they are exist)
			size_t pos = 0, lastPos = 0;
			while ((pos = upt.find_first_of(delims, lastPos)) != string::npos)
			{

				if(pos-lastPos == 0)
				{
					scriptTokens.push_back(upt.substr(lastPos, 1));
					lastPos ++;
				}
				else
				{
					scriptTokens.push_back(upt.substr(lastPos, pos-lastPos));
					lastPos = pos;
				}

			}

			if(lastPos!=upt.length()) scriptTokens.push_back(upt.substr(lastPos));
		}

	}

	return scriptTokens;
}


//-----------------------------------------------------------------------------------------------------------------------
/*RecordInfo InputParser::Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo = findRecordByKeyWord(keyWord, scriptTokens, start, stop);

	

	return rinfo;
}*/


void InputParser::LoadConfigFromFile(GlobalConfig *config, string fileName)
{
	vector<string> tokens = ReadScriptFile(fileName);
	
	Parser *stdParser = new Parser();
 
	// read base parameters
	if(stdParser->readArray<int>(config->spins,"Spins", tokens).type == RecordType::NotFound )
		throw SpinException("error, no 'Spins' record");
		
	config->numberOfSpins = config->spins.size();
	
	stdParser->readVal(config->isStaticCalc,"IsStaticCalc", tokens);
	stdParser->readVal<bool>(config->isDynamicCalc,"IsDynamicCalc", tokens);


	//----------------------------------------------------------------
	// read hamiltonian -----------------------------------------------
	//----------------------------------------------------------------
	HamiltonianParser *hamp = new HamiltonianParser("Hamiltonian");
	HeisOpParser *heisop = new HeisOpParser("Heis");
	AnisOpParser *anisop = new AnisOpParser("Anis");
	ZeemanOpParser *zeemop = new ZeemanOpParser("Zeeman");

	hamp->AddOpParser(heisop);
	hamp->AddOpParser(anisop);
	hamp->AddOpParser(zeemop);
	
	if( hamp->Parse(config,tokens).type == RecordType::NotFound )
		throw SpinException("error, no 'Hamiltonian' record");

	// Parse observbles -----------------------------------------------
	ObservablesParser *obsp = new ObservablesParser("Observables");

	obsp->SetHamiltonianParser(hamp);

	obsp->Parse(config,tokens);

	delete obsp;
	delete hamp;
	delete heisop;
	delete anisop;
	delete zeemop;


	// read parameters data after loading hamiltonian -----------------------------------------------
	ParamParser *pp = new ParamParser("Params");
	ParDataLoader* filedl = new FileParDataLoader("file");
	ParDataLoader* stepdl = new StepParDataLoader("step");
	ParDataLoader* gaussdl = new GaussParDataLoader("gauss");

	pp->AddParDataLoader(filedl);
	pp->AddParDataLoader(stepdl);
	pp->AddParDataLoader(gaussdl);

	pp->Parse(config,tokens);

	delete gaussdl;
	delete stepdl;
	delete filedl;
	delete pp;


	// parse density matrix -----------------------------------------------
	DensityMatrixParser dmp("DensityMatrix");

	dmp.Parse(config,tokens);


	// check some things -----------------------------------------------
	if( !config->isStaticCalc && !config->isDynamicCalc )
	{
		throw SpinException("error, calculation should be either static or dynamic, specify 'isStaticCalc' or 'isDynamicCalc' to 'True' ");
	}

	if( config->isStaticCalc && config->isDynamicCalc )
	{
		throw SpinException("error (temporary unavailable), calculation can not be static and dynamic at the same time");
	}

	// read config for static calc -----------------------------------------------
	if(config->isStaticCalc)
	{
		RecordInfo statri = stdParser->findRecordByKeyWord("StaticCalc",tokens);
		if(statri.type == RecordType::NotFound)
			throw SpinException("error, no 'StaticCalc' record");

		// read basic for static calc
		if(stdParser->readVal<int>(config->statConfig.numEig,"NumEig",tokens, statri.out_start, statri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'NumEig' in 'StaticCalc' record");

		if(stdParser->readVal<string>(config->statConfig.magMomOutFile ,"OutMagMomFile",tokens, statri.out_start, statri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'OutMagMomFile' in 'StaticCalc' record");

		if(stdParser->readVal<string>(config->statConfig.eigValOutFile ,"OutEigValFile",tokens, statri.out_start, statri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'OutEigValFile' in 'StaticCalc' record");

		// read additional parameters
		stdParser->readVal<bool>(config->statConfig.writeAllMagMom ,"WriteMagMomPerEigV",tokens, statri.out_start, statri.out_end );

		stdParser->readVal<double>(config->statConfig.temperature ,"Temperature",tokens, statri.out_start, statri.out_end );

		// if we write mag mom per eigv
		if(config->statConfig.writeAllMagMom)
		{
			if(stdParser->readVal<string>(config->statConfig.magMomPerEigFile ,"OutMagMomPerEigVFile",tokens, statri.out_start, statri.out_end ).type == RecordType::NotFound)
				throw SpinException("error, no 'OutMagMomPerEigVFile' in 'StaticCalc' record");
		}

		// arrays of out magmoms, little bit stupid, but...
		vector<int> snums;
		stdParser->readArray<int>(snums ,"OutSpinNums",tokens, statri.out_start, statri.out_end );

		config->statConfig.spinNumsForMagMom.clear();

		for(auto sn: snums)
		{
			config->statConfig.spinNumsForMagMom.push_back(vector<int>(1,sn));
		}

		// spin components
		// they will be by default 0 1 2
		// TODO make it right later
		config->statConfig.spinComponents={0,1,2};

		config->statConfig.isConfigured=true;
	}

	// read config for dynamic calc -----------------------------------------------
	if(config->isDynamicCalc)
	{
		RecordInfo dynri = stdParser->findRecordByKeyWord("DynamicCalc",tokens);
		if(dynri.type == RecordType::NotFound)
			throw SpinException("error, no 'DynamicCalc' record");

		// read basic
		if(stdParser->readVal<double>(config->dynConfig.timeEnd ,"MaxTime",tokens, dynri.out_start, dynri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'MaxTime' in 'DynamicCalc' record");

		if(stdParser->readVal<double>(config->dynConfig.timeStep ,"TimeStep",tokens, dynri.out_start, dynri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'TimeStep' in 'DynamicCalc' record");

		if(stdParser->readVal<double>(config->dynConfig.damping ,"Damping",tokens, dynri.out_start, dynri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'Damping' in 'DynamicCalc' record");

		stdParser->readVal<int>(config->dynConfig.initEigState ,"InitEigVNum",tokens, dynri.out_start, dynri.out_end );

		if(stdParser->readVal<string>(config->dynConfig.magMomOutFile ,"OutFile",tokens, dynri.out_start, dynri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'OutMagMomFile' in 'DynamicCalc' record");

		if(stdParser->readVal<string>(config->dynConfig.observablesFile ,"ObservablesFile",tokens, dynri.out_start, dynri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'ObservablesFile' in 'DynamicCalc' record");

		stdParser->readVal<string>(config->dynConfig.entropyFile ,"EntropyFile",tokens, dynri.out_start, dynri.out_end );

		config->dynConfig.saveEveryPS = config->dynConfig.timeStep * 5;
		stdParser->readVal<double>(config->dynConfig.saveEveryPS ,"SaveEvery",tokens, dynri.out_start, dynri.out_end );

		config->dynConfig.spinNumsForMagMom.clear();
		if(stdParser->readArray<int>(config->dynConfig.spinNumsForMagMom ,"OutSpinNums",tokens, dynri.out_start, dynri.out_end ).type == RecordType::NotFound)
			throw SpinException("error, no 'OutSpinNums' in 'DynamicCalc' record");

		config->dynConfig.isConfigured=true;
	}
		

	delete stdParser;
}
}
