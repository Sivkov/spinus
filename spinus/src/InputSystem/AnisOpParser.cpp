/*
 * AnisOpParser.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: isivkov
 */


#include <iostream>

#include "AnisOpParser.h"
#include "../IonAnisQOp.h"

namespace sps
{


//----------------------------------------------------------------------------------------------------
RecordInfo AnisOpParser::readLineRecord(GlobalConfig *config, RecordInfo inpinfo)
{
	vector<string>::iterator it = inpinfo.out_start;

	if(inpinfo.type == RecordType::NotFound || inpinfo.type == RecordType::ReadError)
		return inpinfo;

	// read operator record to measure size (may be it is stupid)
	vector<string> subv(inpinfo.out_start,inpinfo.out_end);

	if(subv.size() < minRecLength  ) // because minimum format is  'heis 0 1 J;'
	{
		throw SpinException("AnisOpParser: error in Anisotropy operator, to few parameters: ... '" + *it + "' ...");
	}

	int ind=0;
	int ind2=0;
	double D=1.0;
	double E=1.0;
	string parD;
	string parE;
	bool isParD=false;
	bool isParE=false;

	// read spin index
	readValFromToken<int>(*(++it), ind);

	if(subv.size()==minRecLength+1) // we second second index, which means that we need to create several operators
	{
		readValFromToken<int>(*(++it), ind2);

		if(ind == ind2 || ind < 0 || ind2 < 0)
		{
			throw SpinException("AnisOpParser: error in Anisotropy operator, spin indices are wrong: ... '" + *it + "' ...");
		}
	}

	// try to read parameterized parameters :)
	// if it is number - fine, if not - we need to make parameterized operator
	parD=*(++it);
	isParD = readParamFromToken<double>(parD,D);

	parE=*(++it);
	isParE = readParamFromToken<double>(parE,E);

	// check if 4th parameter is exist
	++it;

	if(subv.size()==minRecLength && ((*it)==lineCloseChar || (*it)==blockCloseChar) ) // create only one operator
	{
		createSingleOp(config, ind, D, E, isParD, isParE , parD, parE);
		return inpinfo;
	}

	if(subv.size()==minRecLength+1 && ((*it)==lineCloseChar || (*it)==blockCloseChar)) // range of spins
	{
		createRangeSpinsOP(config, ind, ind2, D, E, isParD, isParE , parD, parE);
		return inpinfo;
	}

	// if we are here, then something seriously wrong
	throw SpinException("AnisOpParser: error, Anisotropy operator is wrong, something missed: ... '" + *(it-2) + " " + *(it-1) + *it + "' ...");
	return inpinfo;
}



//----------------------------------------------------------------------------------------------------
void AnisOpParser::createRangeSpinsOP(GlobalConfig *config, int ind, int ind2, double D, double E, bool isParD, bool isParE, string parD, string parE)
{
	VarParameter::State parStateD = VarParameter::Dynamic;
	VarParameter::State parStateE = VarParameter::Dynamic;

	if(!isParD)
	{
		parD = defaultParName;
		parStateD = VarParameter::Fixed;
	}

	if(!isParE)
	{
		parE = defaultParName;
		parStateE = VarParameter::Fixed;
	}

	VarParameter* pD = getVarParam(config, parD, parStateD);
	VarParameter* pE = getVarParam(config, parE, parStateE);

	int inc = ind<ind2 ? 1 : -1;



	AbstractStore<QOperator> *store = StoreLocator<QOperator>::GetStore();

	for(int i = ind; i != ind2+inc; i+=inc)
	{
		IonAnisQOp *op = new IonAnisQOp(i, config->numberOfSpins, D, E, "anis");
		StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

		config->operators.push_back(op);

		vector<Tensor*> comp = op->GetComponents(0);
		pD->AddTensor(comp);

		comp = op->GetComponents(1);
		pE->AddTensor(comp);
		comp = op->GetComponents(2);
		pE->AddTensor(comp);
	}
}


//----------------------------------------------------------------------------------------------------
void AnisOpParser::createSingleOp(GlobalConfig *config, int ind, double D, double E, bool isParD, bool isParE, string parD, string parE)
{
	// create operator
	IonAnisQOp *op = new IonAnisQOp(ind, config->numberOfSpins, D, E, "anis") ;
	StoreLocator<QOperator>::GetStore()->Add(op); // add new operators to store

	config->operators.push_back(op);

	// set up D parameter
	VarParameter::State parState = VarParameter::Dynamic;

	if(!isParD)
	{
		parD = defaultParName;
		parState = VarParameter::Fixed;
	}

	VarParameter* pD = getVarParam(config, parD, parState);
	vector<Tensor*> comp = op->GetComponents(0);
	pD->AddTensor(comp);

	if(!isParE)
	{
		parE = defaultParName;
		parState = VarParameter::Fixed;
	}

	VarParameter* pE = getVarParam(config, parE, parState);
	comp = op->GetComponents(1);
	pE->AddTensor(comp);
	comp = op->GetComponents(2);
	pE->AddTensor(comp);
}

}

