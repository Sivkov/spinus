/*
 * StepParDataLoader.h
 *
 *  Created on: Jan 15, 2016
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_STEPPARDATALOADER_H_
#define INPUTSYSTEM_STEPPARDATALOADER_H_

#include "ParDataLoader.h"

namespace sps
{


class StepParDataLoader: public ParDataLoader
{
public:
	StepParDataLoader(string keyWord) : ParDataLoader(keyWord){}

	RecordInfo LoadParData(VarParameter *par, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
	{
		Parser* parser = GetParser();

		vector<double> params; // params = t0, tw, p1, p2, step

		RecordInfo rinfo = parser->readArray(params,parser->GetKeyWord(), scriptTokens, start, stop);

		if(params.size() < 5)
			throw SpinException("Step data record has wrong number of parameters");

		int n = (params[0]+params[1]) / params[4] + 1; // (t0 + tw) / step + 1

		vector<double> pdat; // *(par->GetParDataUnsafe());

		for(int i = 0; i < n; i++)
		{
			pdat.push_back(slowStep(i*params[4], params));
		}

		par->SetParData(std::move(pdat));
		par->SetArgStep(params[4]);

		return rinfo;
	}

private:


	double slowStep(double t, vector<double> &pv) //pv: t0, tw, p1, p2
	{
		if(t<pv[0]-pv[1]) return pv[2];
		if(t>pv[0]+pv[1]) return pv[3];

		return pv[2]+(t-(pv[0]-pv[1])) * (pv[3]-pv[2]) / (2 * pv[1]);
	}


};

}


#endif /* INPUTSYSTEM_STEPPARDATALOADER_H_ */
