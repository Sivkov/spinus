/*
 * FileParDataLoader.h
 *
 *  Created on: Jan 15, 2016
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_FILEPARDATALOADER_H_
#define INPUTSYSTEM_FILEPARDATALOADER_H_

#include <iostream>
#include <sstream>
#include "ParDataLoader.h"

namespace sps
{


class FileParDataLoader: public ParDataLoader
{
public:
	FileParDataLoader(string keyWord) : ParDataLoader(keyWord){}

	RecordInfo LoadParData(VarParameter *par, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
	{
		Parser* parser = GetParser();

		string fname; // params = t0, tw, p1, p2, step

		RecordInfo rinfo = parser->readVal<string>(fname, parser->GetKeyWord(), scriptTokens, start, stop);

		if(rinfo.type != RecordType::Line)
			throw SpinException("ParamParser: error in '" +parser->GetKeyWord()+ "' record , parameter data file of parameter '" + par->GetName() + "' is not specified");

		ifstream file(fname);

		if(!file.is_open())
			throw SpinException("ParamParser: error in '" +parser->GetKeyWord()+ "' record , cannot find '" + fname + "' file");

		// check number of columns -----------------------------------------------------------
		string line;
		std::getline(file, line);

		stringstream ss;
		ss.str(line);
		string upt;
		int i=0;

		while((ss>>upt))
		{
			i++;
		}

		if(i>2)
		{
			throw SpinException("Wrong number of columns in file '"+fname+"'");
		}

		// read file ---------------------------------------------------------------------------
		vector<double> pdat;
		vector<double> time;

		double param=0.0;

		while( (file>>param) )
		{
			if(i==2)
			{
				time.push_back(param);

				file>>param;
				pdat.push_back(param);
			}

			if(i==1)
			{
				pdat.push_back(param);
			}
		}

		file.close();

		par->SetParData(std::move(pdat));

		if(i==2 && time.size()>1)
			par->SetArgStep(time[1]-time[0]);
		else
			par->SetArgStep(0);

		return rinfo;
}

private:


};

}



#endif /* INPUTSYSTEM_FILEPARDATALOADER_H_ */
