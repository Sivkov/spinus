/*
 * Parser.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: isivkov
 */

#include "Parser.h"
#include <iostream>
namespace sps
{
//----------------------------------------------------------------------------------------------------
RecordInfo Parser::readTillLineCloseChar(vector<string>& scriptTokens, vector<string>::iterator start)
{
	RecordInfo rinfo{RecordType::NotFound,start,scriptTokens.end()};

	vector<string>::iterator it = find_if(start, scriptTokens.end(),
			[this](string &tok)
			{
				return (tok == lineCloseChar);
			}
	);

	if(it==scriptTokens.end())
	{
		return rinfo;
	}

	rinfo.type = RecordType::Line;
	rinfo.out_start = start;
	rinfo.out_end = it+1;
	rinfo.name = *start;

	return rinfo;
}


//----------------------------------------------------------------------------------------------------
RecordInfo Parser::readBlock(vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo{RecordType::NotFound,start,stop};

	// find begin of block
	vector<string>::iterator it = find_if(start, stop, [this](string &tok){	return (tok == blockOpenChar);} );

	// not found
	if(it==stop)
	{
		return rinfo;
	}


	// if begin is not begin of the whole script then we can have a name of block
	if(it != scriptTokens.begin())
	{
		string name = *(it-1);

		// if there is a name of a block
		if(name.find_first_of(delimiters) == string::npos )
		{
			rinfo.name = name;
		}
	}

	// search further for block close char
	rinfo.out_start = it-1;

	++it;
	int bracketCounter = 1;


	// check for inner brackets
	while(it != scriptTokens.end() && bracketCounter != 0)
	{
		if( (*it) == blockOpenChar )  bracketCounter++;
		if( (*it) == blockCloseChar ) bracketCounter--;

		++it;
	}

	if(it == scriptTokens.end() && bracketCounter!=0)
	{
		throw SpinException("error, missing '" + blockCloseChar + "' at " + keyWord + " ...");
	}

	rinfo.out_end = it;
	rinfo.type = RecordType::Block;

	return rinfo;
}

//----------------------------------------------------------------------------------------------------
RecordInfo Parser::findRecordByKeyWord(string keyWord, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop)
{
	RecordInfo rinfo{RecordType::NotFound,start,stop};

	vector<string>::iterator it = find_if(start, stop, [&keyWord](string &tok){ return tok == keyWord;} );

	if(it == stop)
	{
		rinfo.out_start = stop;
		return rinfo;
	}

	rinfo.name = keyWord;

	rinfo.out_start = it;

	// check next symbol. if it "{" or not
	++it;

	if((*it) == blockOpenChar) // record is block, find end of block
	{
		// TODO change this part to written function ReadBlock()
		++it;
		int bracketCounter = 1;

		// check for inner brackets
		while(it != scriptTokens.end() && bracketCounter != 0)
		{
			if( (*it) == blockOpenChar ) {bracketCounter++; }
			if( (*it) == blockCloseChar ) {bracketCounter--; }

			++it;
		}

		if(it == scriptTokens.end() && bracketCounter!=0)
		{
			throw SpinException("error, missing '" + blockCloseChar + "' at " + keyWord + " ...");
		}

		rinfo.out_end = it; //TODO change in operators parsers
		rinfo.type = RecordType::Block;
	}
	else // recors is line, find ";"
	{
		it = find_if(it, scriptTokens.end(), [this](string &tok){ return tok == lineCloseChar;} );

		if(it == scriptTokens.end())
		{
			throw SpinException("error, missing '" + lineCloseChar + "' or '" + blockOpenChar + "' at " + keyWord + " ...");
		}

		rinfo.out_end = it+1; //TODO change in operators parsers
		rinfo.type = RecordType::Line;
	}

	return rinfo;
}


//----------------------------------------------------------------------------------------------------
RecordInfo Parser::findRecordByKeyWord(string keyWord, vector<string>& scriptTokens, vector<string>::iterator start)
{
	return findRecordByKeyWord(keyWord, scriptTokens, start, scriptTokens.end());
}



//----------------------------------------------------------------------------------------------------
RecordInfo Parser::findRecordByKeyWord(string keyWord, vector<string>& scriptTokens)
{
	return findRecordByKeyWord(keyWord, scriptTokens, scriptTokens.begin());
}


}
