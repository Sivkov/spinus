/*
 * DensityMatrixParser.h
 *
 *  Created on: Jan 26, 2016
 *      Author: isivkov
 */

#ifndef INPUTSYSTEM_DENSITYMATRIXPARSER_H_
#define INPUTSYSTEM_DENSITYMATRIXPARSER_H_

#include "Parser.h"

using namespace std;

namespace sps
{
class DensityMatrixParser : public Parser
{
public:
	DensityMatrixParser(string keyWord, string singleDM="single", string doubleDM="double"):
		Parser(keyWord),
		singleDM(singleDM),
		doubleDM(doubleDM)
		{}

	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start, vector<string>::iterator stop);
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens, vector<string>::iterator start){ return Parser::Parse(config, scriptTokens,start);}
	virtual RecordInfo Parse(GlobalConfig *config, vector<string>& scriptTokens){ return Parser::Parse(config, scriptTokens);}


protected:

	void checkI(int i, int max)
	{
		if(i<0 || i>=max)
		{
			throw SpinException("Spin index in Density Matrix record must be >= 0 and < number of spins");
		}
	}

	void checkI(int i, int j, int max)
	{
		checkI(i,max);
		checkI(j,max);

		if(i>=j)
		{
			throw SpinException("Spin indices in Density Matrix record must be in ASCENDING order and are NOT equal");
		}
	}

	string singleDM,doubleDM;
};



}


#endif /* INPUTSYSTEM_DENSITYMATRIXPARSER_H_ */
