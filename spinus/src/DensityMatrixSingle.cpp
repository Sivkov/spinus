/*
 * DensityMatrixSingle.cpp
 *
 *  Created on: Jan 23, 2016
 *      Author: isivkov
 */


#include "DensityMatrixSingle.h"
#include <iostream>

namespace sps
{
DensityMatrixSingle::DensityMatrixSingle(JJState *jjs, int spinInd)
:jjs(jjs),
 spinInd(spinInd),
 isConfigured(false)
{
	twoJ=jjs->getInitTwoJ()[spinInd];
	dim=twoJ+1;
	initEmptyDMMatrices();
}


void DensityMatrixSingle::initEmptyDMMatrices()
{
	for(int i=0;i<=twoJ;i++)
	{
		for(int j=0;j<=i;j++)
		{
			int twoMi = 2*i-twoJ;
			int twoMj = 2*j-twoJ;

			dmMatrices.push_back({twoMi,twoMj,nullptr});
		}
	}
}

DensityMatrixSingle::~DensityMatrixSingle()
{
	for(auto m: dmMatrices)
	{
		if(m.val3!=nullptr)
			delete m.val3;
	}
}


void DensityMatrixSingle::CalcDensityMatrix(MKL_Complex16 *eigenVec, int vecLength, MKL_Complex16 *outMatr)
{
	Eigen::setNbThreads(1);

	if(!isConfigured)
	{
		throw SpinException("Density matrix is not configured");
	}

	Eigen::Map<VectorZ> vec(eigenVec,vecLength);

#pragma omp parallel for
	for(int i = 0; i < dmMatrices.size(); i++)
	{
		SparseMatrixZ *mat = dmMatrices[i].val3;

		MKL_Complex16 val = (mat==nullptr ? 0.0 : ((*mat)*vec).dot(vec));

		int indI = (dmMatrices[i].val1+twoJ)/2;
		int indJ = (dmMatrices[i].val2+twoJ)/2;

		outMatr[indI*dim+indJ] = val;

		if(indI!=indJ)
		{
			outMatr[indJ*dim+indI] = std::conj(val); // may be dont need
		}
	}
}

int DensityMatrixSingle::GetDimensions()
{
	return dim;
}

vector<int> DensityMatrixSingle::GetSpinIndices()
{
	return {spinInd};
}

void DensityMatrixSingle::BuildMatrix()
{
	if(jjs==nullptr)
	{
		throw SpinException("JJState is not set to DensityMatrix class");
	}

	if(isConfigured) return;

	for(int k = 0; k <= twoJ; k++)
	{
		for(int q = -k; q <= k; q++)
		{
			SingleTensor tens(spinInd,jjs->GetNumParticles(),2*k,2*q,std::sqrt( (2*k+1) / sqrme (twoJ, 2*k) ) );

			SparseMatrixZ* mat = new SparseMatrixZ(jjs->GetBasisDimension(),jjs->GetBasisDimension());

			MatrixBuilder mb(jjs);

			mb.BuildFromTensorSparse(tens,*mat);

			//cout<<*mat<<endl;

			if(mat->nonZeros()==0) continue;


			double s3j=0;
			for(int i=0;i<dmMatrices.size();i++)
			{

				int twoMi = dmMatrices[i].val1;
				int twoMj = dmMatrices[i].val2;

				int coef = (((twoJ - 2*k + twoMj )>>1)&1) ? -1.0 : 1.0;

				double s3j = std::sqrt(2 * k + 1) * coef * gsl_sf_coupling_3j(twoJ, 2*k, twoJ, twoMi, 2*q, -twoMj);

//				cout<<twoMi<<" "<<twoMj<<" "<<s3j<<" "<<endl;

				if(s3j == 0.0) continue;

				// thread critical begin
				if(dmMatrices[i].val3==nullptr)
				{
					dmMatrices[i].val3 = new SparseMatrixZ(jjs->GetBasisDimension(),jjs->GetBasisDimension());
				}

				*(dmMatrices[i].val3) += (*mat) * s3j;
				// thread critical end



			}
/*
			cout<<dmMatrices.size()<<endl;
			for(auto m: dmMatrices)
			{
				cout<<m.val1<<" "<<m.val2<<"\n"<<endl;
				if(m.val3!=nullptr)
					cout<<*(m.val3)<<endl;
				else
					cout<<0.0<<"\n----------"<<endl;
			}

			cout<<"==============================="<<endl;
*/
			delete mat;
		}
	}

	isConfigured=true;

}
}

