/*
 * StaticCalculator.h
 *
 *  Created on: Jul 10, 2015
 *      Author: isivkov
 */

#ifndef SPINSTATIC_H_
#define SPINSTATIC_H_

#include <vector>
#include <string>
#include <fstream>

#include "SpinSinTypes.h"
#include <mkl.h>

#include "JJState.h"
#include "Tensor.h"
#include "HamiltonianBuilder.h"
#include "SingleTensor.h"
#include "PairTensor.h"

#include "ZeemanQOp.h"
#include "HeisenbergQOp.h"
#include "IonAnisQOp.h"
#include "SpinQOp.h"

#include "MatrixBuilder.h"
#include "VarParameter.h"
#include "SpinStaticConfig.h"
using namespace std;

namespace sps
{


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
class SpinStatic
{
public:
	SpinStatic();
	virtual ~SpinStatic();

	void SetJJState(JJState *jjs);
	void SetHamBuilder(HamiltonianBuilder *hb);

	void SetConfig(SpinStaticConfig &config);
	void SetConfig(SpinStaticConfig &&config);

	void Calc();

protected:
	virtual void userInit();
	virtual void userCalc();

	void outputInit();
	void magMomInit();

	void outputEigVal();
	void outputMagMom();

	void calcTotalMatrix();
	void eigSolve();
	void calcMagMom();
	bool calcSpinOpMatr(int num, int component, MatrixC &spinMat);
	bool calcSpinOpMatr(const vector<int> &spinNums, int component, MatrixC &spinMat);
	double calcZFactor(double energy, double temp);

	bool configured;
	int parLoopIndex;

	// external
	HamiltonianBuilder *hamBuilder;
	JJState *jjState;

	// own config
	SpinStaticConfig config;


	// spin ops for magn mom calc
	vector<QOperator*> spinOps;

	long int basisDim;

	// vectors filled per ieration
	// out magmom
	int numOutSpins;
	int numOutSComps;

	vector<double> curMagMoms;
	vector<double> curMagMomsEig;

	//vector<vector<double>> curMagMoms
	//vector<vector<vector<double>>> curMagMomsEig;

	vector<MKL_Complex16> curParams;
	vector<double> curEigVals;
	vector<MKL_Complex16> curEigVecs;

	// to account temperature distribution
	vector<double> Zfactors;
	double Zsum;

	// store parameterized parts of Hamiltonian for quick access
	vector<MKL_Complex16*> parMatDatas;

	// build spin ops on fly
	MatrixBuilder spinMatBldr;

	// total matrix
	MatrixC totmat;
	MatrixC spinmat;

	// MKL setups
	int numeigfnd;
	vector<int> isuppz; 				// = new int[2 * numeig];
	vector<MKL_Complex16> tmpvec2;		// = new MKL_Complex16[dim];
};

}



#endif /* SPINSTATIC_H_ */
