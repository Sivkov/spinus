/*
 * SpinException.h
 *
 *  Created on: Dec 11, 2014
 *      Author: isivkov
 */

#ifndef SPINEXCEPTION_H_
#define SPINEXCEPTION_H_

#include <stdexcept>
#include <exception>
#include <string>

namespace sps
{

class SpinException : public std::exception
{
public:
	SpinException(){};
	SpinException(const std::string &reason){this->reason=reason;}
	SpinException(std::string &&reason){this->reason=reason;}

	virtual ~SpinException() throw() {}

	virtual const char* what() const throw (){return reason.c_str();}

protected:
	std::string reason;

};



}


#endif /* SPINEXCEPTION_H_ */
