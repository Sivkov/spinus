/*
 * ZeemanQOp.h
 *
 *  Created on: Jun 9, 2015
 *      Author: isivkov
 */

#ifndef ZEEMANQOP_H_
#define ZEEMANQOP_H_

#include <cmath>
#include "QOperator.h"
#include "SingleTensor.h"
#include "SpinSinTypes.h"

namespace sps
{

class ZeemanQOp: public QOperator
{
public:
	ZeemanQOp(int numS=0,
			int numTot=1,
			double Bx=1., double By=1., double Bz=1., string name="")
	: QOperator(name), Bx(Bx), By(By), Bz(Bz)
	{

		tensors = std::vector<Tensor*>(5,nullptr);

		MKL_Complex16 im = {0,1};

		// factor mu * g
		double factor = 2.0 * 5.8 * 0.01;

		// Tx = -1/sqrt(2) * ( T1 - Tm1 )
		if(Bx!=0)
		{
			tensors[0] = new SingleTensor(numS, numTot, 2, 2,  1/std::sqrt(2.0) * Bx , "zeem +1x");
			tensors[1] = new SingleTensor(numS, numTot, 2,-2, -1/std::sqrt(2.0) * Bx , "zeem -1x");
		}

		// Ty = i/sqrt(2) * ( T1 + Tm1 )
		if(By!=0)
		{
			tensors[2] = new SingleTensor(numS, numTot, 2, 2, - im/std::sqrt(2.0) * By , "zeem +1y");
			tensors[3] = new SingleTensor(numS, numTot, 2,-2, - im/std::sqrt(2.0) * By , "zeem -1y");
		}

		//Tz=T0
		if(Bz!=0)
		{
			tensors[4] = new SingleTensor(numS, numTot, 2, 0, - Bz , "zeem z");
		}

		// add new tensors in store
		StoreLocator<Tensor>::GetStore()->Add(tensors);
	}

	virtual std::vector<Tensor*> GetComponents(int num)
		{
			switch(num)
			{
			case 0:
				return std::vector<Tensor*>(tensors.begin(), tensors.begin()+2 );
				break;

			case 1:
				return std::vector<Tensor*>(tensors.begin()+2, tensors.begin()+4 );
				break;

			case 2:
				return std::vector<Tensor*>(1, tensors[4] );
				break;

			default:
				throw SpinException("ZeemanQOp has no component number > 2");
				return std::vector<Tensor*>();
				break;
			}
		}



protected:
	double Bx,By,Bz;
};

}


#endif /* ZEEMANQOP_H_ */
