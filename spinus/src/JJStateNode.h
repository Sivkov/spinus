/*
 * JJStateNode.h
 *
 *  Created on: Dec 5, 2014
 *      Author: isivkov
 */

#ifndef JJSTATENODE_H_
#define JJSTATENODE_H_

#include <vector>

namespace sps
{

class JJState;

class JJStateNode
{
	friend class JJState;
public:
	JJStateNode(int ownTwoJ);
	//JJStateNode(int ownTwoJ, int plusTwoJ, vector<JJStateNode*>& subStateNodes);

	//void SetPlusJ(int plusTwoJ, vector<JJStateNode*>& subStateNodes);

	int GetOwnTwoJ();
	int GetPlusTwoJ();
	std::vector<const JJStateNode*> GetSubNodes();
	const JJStateNode* GetLvlUpNode();
	std::vector<int> GetJJConfiguration();

private:
	int ownTwoJ;
	int plusTwoJ;
	std::vector<JJStateNode*> sumTwoJ;
	JJStateNode *lvlUp;
};


}

#endif /* JJSTATENODE_H_ */
