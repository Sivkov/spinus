# SPINUS #

### spin dynamics code with a relaxation term ###

### What is this repository for? ###

The code is aimed to 

- calculate a quantum spin dynamics with damping term (solves the time-dependent Schroedinger equation) 

- calculate spin spectrum 

it uses sparse matrices and soon it will be fully sparse.


### How do I get set up? ###

Download or clone 'gcc' branch

To compile you need the libraries/compilers:
MKL, TBB, GSL, gcc5(tested)

then

- setup the libraries in config.mk in the root directory

- in the directory spinus/Debug or spinus/Release type 'make all' to compile libspinus.a

- in the spinus_dsk/Debug or spinus_dsk/Release type 'make all' to compile desktop executable

- in the directory spinus_dsk/tests you can find the tests with the input file examples

### Who do I talk to? ###

in case of questions

sivkov1987@yandex.ru