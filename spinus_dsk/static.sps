

Spins 3 3 3 3; # 3/2 3/2 3/2 3/2


IsStaticCalc true;

Params
{
	Bz zfield.dat;
}

Hamiltonian
{
	Heis 	0 3 0.25 chain; # 0-1 1-2 2-3
	Heis 	0 3 0.25; # 0-3
	Heis 	0 2 0.1; # 0-2
	Heis 	1 3 0.1; # 1-3
	
	Anis 	0 3	0.18	0.0; # 0 1 2 3
	Zeeman 	0 3 	0 	0 	Bz; # 0 1 2 3
}

StaticCalc
{
	NumEig 8;
	OutMagMomFile stat_mm.dat;
	OutEigValFile stat_ev.dat;
	Temperature 0;
	OutSpinNums 0 1 2 3;
}

