

Spins 2;


IsDynamicCalc true;

Params
{
	Bx Gauss_pulse.dat;
}

Hamiltonian
{
	
	Anis 	0 -0.1		0.0;
	Zeeman 	0 	Bx 	0 	0.3;
}

DynamicCalc
{
	MaxTime 	100;	#ps
	TimeStep 	0.02;	#ps
	SaveEvery 	0.08;	#ps
	Damping 	0.2;	
	InitEigVNum 0;
	OutFile 		out.dat;
	OutSpinNums 0;
}

