

Spins 1 1;


IsDynamicCalc true;

Params
{
	Bx gauss  30 5 0.5 0.02;
	Bz step 10 5 -1.5 1.5 0.02;
}

Hamiltonian
{
	Heis 	0 1 -0.022 chain;	
	Anis 	0 1	-1.0	0.0;
	Zeeman 	0 1  Bx 	0 	0;
	Zeeman  0    0  	0       Bz;
}

DynamicCalc
{
	MaxTime 	1000;	#ps
	TimeStep 	0.02;	#ps
	SaveEvery 	0.2;	#ps
	Damping 	0.05;	
	InitEigVNum 0;
	OutFile 		test_S0.5.dat;
	OutSpinNums 0 1;
	ObservablesFile	observ_S0.5.dat
}


Observables
{
TotExc{ 	Heis 	0 1 	-0.022 chain;	}
TotAnis{	Anis 	0 1		-1.0	0.0; 	}
}
