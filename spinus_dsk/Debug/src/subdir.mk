################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/SpinSin.cpp 

OBJS += \
./src/SpinSin.o 

CPP_DEPS += \
./src/SpinSin.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	icc -openmp -std=c++0x -I"/home/isivkov/workspace/spinsin/spinus/src" -I/opt/intel/composer_xe_2013_sp1.0.080/compiler/include -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include -I/cluster/intel_2013SP1/composer_xe_2013_sp1.0.080/compiler/include -I/cluster/intel_2013SP1/composer_xe_2013_sp1.0.080/mkl/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


