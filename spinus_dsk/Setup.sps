

Spins 3 3 3;


IsDynamicCalc true;

Params
{
	Bx gauss  100 10 0.5 0.02;
	Bz step 30 10 -1.5 1.5 0.02;
}

DensityMatrix
{
single 0 2;
double 0 2 1;
}


Hamiltonian
{
	Heis 	0 2 -0.268 chain;	
	Anis 	0 2	-0.44	0.0;
	Zeeman 	0   Bx 	0 	Bz;
}

DynamicCalc
{
	MaxTime 	1000;	#ps
	TimeStep 	0.02;	#ps
	SaveEvery 	0.4;	#ps
	Damping 	0.05;	
	InitEigVNum 0;
	OutFile 		test.dat;
	OutSpinNums 	0 1 2;
	ObservablesFile	observ.dat
	EntropyFile 	entropy.dat
}


Observables
{
TotExc{ 	Heis 	0 2 	-0.268 chain;	}
TotAnis{	Anis 	0 2		-0.44	0.0; 	}
}
