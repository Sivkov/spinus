//============================================================================
// Name        : sparse_test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <mkl.h>

#include <iostream>
#include <vector>
#include <MatrixSparse.h>
#include <string>

#include <spinus.h>

using namespace Eigen;
using namespace std;
using namespace sps;

void test_sparse_matrix_builder();
void test_sparse_matrix_builder_big_matr();
void test_density_matrix();
void test_eigv_calc();

int main(void) {

	cout<<sizeof(SparseMatrixZ)<<endl;
//	test_sparse_matrix_builder();
//	test_sparse_matrix_builder_big_matr();
	test_density_matrix();
}

void test_sparse_matrix_builder()
{
	cout<<"##### test 1 spin S=1 #####"<<endl;
	cout<<"##### There should be spin matrices for S=1 #####"<<endl;
	JJState jjs(2);
	//jjs.PlusJ(2);

	jjs.BuildBasis();

	MatrixBuilder mb;

	mb.SetJJState(&jjs);

	int n = jjs.GetNumParticles();

	SingleTensor t10(0,n,2,0,1.0) ,t11(0,n,2,2,1.0), t1m1(0,n,2,-2,1.0);


	SparseMatrixZ sm11;
	SparseMatrixZ sm10;
	SparseMatrixZ sm1m1;

	mb.BuildFromTensorSparse(t10,sm10);
	cout<<sm10<<endl;

	mb.BuildFromTensorSparse(t11,sm11);
	cout<<sm11<<endl;

	mb.BuildFromTensorSparse(t1m1,sm1m1);
	cout<<sm1m1<<endl;

/*
	MKL_Complex16 dm10={1,0,0,
						0,0,0,
						0,0,1};

	MKL_Complex16 dm10={0,0,0,
						-1,0,0,
						0,-1,0};

	MKL_Complex16 dm10={0,1,0,
						0,0,1,
						0,0,0};

	vector<TripletZ> tv10={TripletZ(0,0,1),TripletZ(2,2,1)};
	vector<TripletZ> tv11={TripletZ(1,0,-1),TripletZ(2,1,-1)};
	vector<TripletZ> tv1m1={TripletZ(0,1,1),TripletZ(1,2,1)};

	SparseMatrixZ sm10check;
	SparseMatrixZ sm11check;
	SparseMatrixZ sm1m1check;

	sm10check.setFromTriplets(tv10.begin(),tv10.end());
	sm11check.setFromTriplets(tv11.begin(),tv11.end());
	sm1m1check.setFromTriplets(tv1m1.begin(),tv1m1.end());

	sm10.isRValue();
	//if(sm10==sm10check ) cout<<"PASSED"<<endl;
	 */

}



void test_sparse_matrix_builder_big_matr()
{
	cout<<"##### test big matrices, calc spin operators for 1st spin S=5/2. N=4#####"<<endl;
	JJState jjs(5);
	jjs.PlusJ(5);
	jjs.PlusJ(5);
	jjs.PlusJ(5);
	jjs.PlusJ(5);
//	jjs.PlusJ(5);


	jjs.BuildBasis();

	MatrixBuilder mb;

	mb.SetJJState(&jjs);

	int n = jjs.GetNumParticles();

	SingleTensor t10(0,n,2,0,1.0) ,t11(0,n,2,2,1.0), t1m1(0,n,2,-2,1.0);


	SparseMatrixZ sm11;
	SparseMatrixZ sm10;
	SparseMatrixZ sm1m1;


	mb.BuildFromTensorSparse(t10,sm10);
	cout<<"nnz s10: "<<sm10.nonZeros()<<endl;

	mb.BuildFromTensorSparse(t11,sm11);
	cout<<"nnz s11: "<<sm11.nonZeros()<<endl;

	mb.BuildFromTensorSparse(t1m1,sm1m1);
	cout<<"nnz s1m1: "<<sm1m1.nonZeros()<<endl;
}



void test_density_matrix()
{
	JJState jjs(2);
	jjs.PlusJ(1);

	jjs.BuildBasis();

	sps::DensityMatrixSingle DM(&jjs,0);

	DM.BuildMatrix();

	vector<MKL_Complex16> dm(9);
	vector<MKL_Complex16> evec(jjs.GetBasisDimension());

	evec[1]=1/std::sqrt(2);
	evec[5]=MKL_Complex16(0,1)/std::sqrt(2);





	cout<<"eigenvector"<<endl;
	for(auto v: evec) cout<<v<<" ";
	cout<<endl;

	cout<<endl;



	cout<<"dm1:"<<endl;
	DM.CalcDensityMatrix(evec.data(),evec.size(),dm.data());

	for(int i = 0; i<3; i++)
	{
		for(int j = 0; j<3; j++)
		{
			cout<<dm[i*3+j]<<" ";
		}
		cout<<endl;
	}

	cout<<endl;




	sps::cvector_triplet_IIpSM dmms = DM.GetMatrices();

	vector<MKL_Complex16> ev(3,0);

	cout<<"Entropy "<<sps::calcVonNeumanEntropy(dm.data(),3,ev.data())<<endl;

	cout<<endl;




	cout<<"dm2:"<<endl;

	sps::DensityMatrixSingle DM2(&jjs,1);

	DM2.BuildMatrix();

	vector<MKL_Complex16> dm2(4);

	DM2.CalcDensityMatrix(evec.data(),evec.size(),dm2.data());

	for(int i = 0; i<2; i++)
	{
		for(int j = 0; j<2; j++)
		{
			cout<<dm2[i*2+j]<<" ";
		}
		cout<<endl;
	}

	cout<<endl;





	vector<MKL_Complex16> ev2(2,0);

	cout<<"Entropy "<<sps::calcVonNeumanEntropy(dm2.data(),2,ev2.data())<<endl;

	cout<<endl;




	cout<<"dm1*dm2:"<<endl;

	vector<MKL_Complex16> dmd(36);

	sps::DensityMatrixDouble DMD(&DM,&DM2);

	DMD.CalcDensityMatrix(evec.data(),evec.size(),dmd.data());

	for(int i = 0; i<6; i++)
	{
		for(int j = 0; j<6; j++)
		{
			cout<<dmd[i*6+j]<<" ";
		}
		cout<<endl;
	}

}



void test_eigv_calc()
{

}
