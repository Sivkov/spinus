################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/test_input.cpp 

OBJS += \
./src/test_input.o 

CPP_DEPS += \
./src/test_input.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	icc -openmp -std=c++11 -I"/home/isivkov/workspace/spinsin/spinus/src" -I/opt/intel/composer_xe_2013_sp1.0.080/compiler/include -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


