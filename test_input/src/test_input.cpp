//============================================================================
// Name        : test_input.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <spinus.h>

using namespace std;
using namespace sps;



//--------------------------------------------------------------------------------
void test_read_toks()
{
	cout<<"---------tokens"<<endl;

	InputParser prs;

	vector<string> tokens = prs.ReadScriptFile("fuu");


	for(auto &s: tokens)
	{
		cout<<s<<endl;
	}
}



//--------------------------------------------------------------------------------
void test_parse_val()
{
	cout<<"--------- int"<<endl;
	InputParser prs;

	vector<string> tokens = prs.ReadScriptFile("fuu");

	Parser p("fuu");

	double a;

	if( p.readVal(a,"MaxTime",tokens).type == RecordType::ReadError ) cout<<"error"<<endl;



	cout<<"MaxTime "<<a<<endl;
}



//--------------------------------------------------------------------------------
void test_parse_array()
{
	cout<<"--------- int array"<<endl;

	InputParser prs;

	vector<string> tokens = prs.ReadScriptFile("fuu");

	Parser p("fuu");

	vector<int> a;
	if( (p.readArray<int>(a,"Spins",tokens)).type == RecordType::NotFound ) cout<<"not found"<<endl;



	for(auto v: a) cout<<v<<" ";
	cout<<endl;
}


//--------------------------------------------------------------------------------
void test_parse_bool()
{
	cout<<"--------- bool"<<endl;
	InputParser prs;

	vector<string> tokens = prs.ReadScriptFile("fuu");

	Parser p("fuu");

	bool a;

	if( (p.readVal(a,"IsDynamicCalc",tokens)).type == RecordType::ReadError ) cout<<"error"<<endl;



	cout<<"IsDynamicCalc "<<a<<endl;
}

//--------------------------------------------------------------------------------
void test_read_block()
{
	cout<<"################ Read Block ################"<<endl;

	InputParser prs;

	vector<string> tokens = prs.ReadScriptFile("fuu");

	Parser p("fuu");

	try
	{
		cout<<"read Observables..."<<endl;
		RecordInfo riObs = p.findRecordByKeyWord("Observables",tokens );

		if( riObs.type == RecordType::NotFound ) throw SpinException("Observables not found");

		cout<<"read 1st block..."<<endl;
		RecordInfo ri = p.readBlock(tokens,riObs.out_start + 2,riObs.out_end);

		if( ri.type == RecordType::NotFound ) throw SpinException("1st record not found");

		cout<<"found block: "<<ri.name<<endl;

		cout<<"read 2st block..."<<endl;
		ri = p.readBlock(tokens,ri.out_end, riObs.out_end);

		if( ri.type == RecordType::NotFound ) throw SpinException("2nd record not found");

		cout<<"found block: "<<ri.name<<endl;

	}
	catch(SpinException &ex)
	{
		cout<< ex.what()<<endl;
	}

}


//--------------------------------------------------------------------------------
void test_read_heis()
{
	cout<<"################ read heis ################"<<endl;


	GlobalConfig cfg;

	HeisOpParser hprs("Heis");
	Parser p("standart");

	InputParser prs;

	vector<string> tokens;

	try
	{
		//--------------------------------------------
		tokens= prs.ReadScriptFile("fuu");

		//--------------------------------------------
		cout<<"read spins"<<endl;
		if( (p.readArray<int>(cfg.spins,"Spins",tokens)).type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cfg.numberOfSpins = cfg.spins.size();

		//--------------------------------------------
		cout<<"read heis operators"<<endl;

		cout<<"op 1"<<endl;
		RecordInfo rinf = hprs.Parse(&cfg,tokens,tokens.begin());
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;


		cout<<"op 2"<<endl;
		rinf = hprs.Parse(&cfg,tokens,rinf.out_end);
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cout<<"op 3"<<endl;
		rinf = hprs.Parse(&cfg,tokens,rinf.out_end);
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;


	}
	catch(exception &ex)
	{
		cout<< ex.what()<<endl;
	}

	if( cfg.parameters.size() == 1) cout<<"parameters size "<< cfg.parameters.size() << " , PASSED"<<endl;
	else cout<<"parameters size "<< cfg.parameters.size() <<" , FAIL"<<endl;

	if( cfg.operators.size() == 6) cout<<"operators size "<< cfg.operators.size() << " PASSED"<<endl;
	else cout<<"operators size "<< cfg.operators.size() <<" , FAIL"<<endl;

	for(auto op: cfg.operators)
	{
		cout<<"### Operator: " << op->GetName() <<endl;
		vector<Tensor*> tens = op->GetAllTensors();

		for(auto t: tens)
		{
			if(t==nullptr)
			{
				cout<<"tensor is null, it could be"<<endl;
				continue;
			}
			for(auto j: t->getInitTwoJ())
				cout<<j<<" ";

			cout<<endl;

			for(auto j: t->getAddTwoJ())
				cout<<j<<" ";

			cout<<endl<<"------------"<<endl;
		}

		cout<<endl;
	}

	cout<<"DONE!"<<endl;
}


//--------------------------------------------------------------------------------
void test_read_anis()
{
	cout<<"################ read anis ################"<<endl;


	GlobalConfig cfg;

	AnisOpParser hprs("Anis");
	Parser p("standart");

	InputParser prs;

	vector<string> tokens;

	try
	{
		//--------------------------------------------
		tokens= prs.ReadScriptFile("fuu");


		//--------------------------------------------
		cout<<"read spins"<<endl;
		if( (p.readArray<int>(cfg.spins,"Spins",tokens)).type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cfg.numberOfSpins = cfg.spins.size();

		//--------------------------------------------
		cout<<"read heis operators"<<endl;

		cout<<"op 1"<<endl;
		RecordInfo rinf = hprs.Parse(&cfg,tokens,tokens.begin());
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cout<<"op 2"<<endl;
		rinf = hprs.Parse(&cfg,tokens,rinf.out_end);
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

	}
	catch(exception &ex)
	{
		cout<< ex.what()<<endl;
	}

	if( cfg.parameters.size() == 1) cout<<"parameters size "<<cfg.parameters.size()<<", PASSED"<<endl;
	else cout<<"parameters size "<< cfg.parameters.size() <<" , FAIL"<<endl;

	if( cfg.operators.size() == 4) cout<<"operators size "<<cfg.operators.size()<<", PASSED"<<endl;
	else cout<<"operators size "<< cfg.operators.size() <<" , FAIL"<<endl;

	for(auto op: cfg.operators)
	{
		cout<<"### Operator: " << op->GetName() <<endl;
		vector<Tensor*> tens = op->GetAllTensors();

		for(auto t: tens)
		{
			if(t==nullptr)
			{
				cout<<"tensor is null, it could be"<<endl;
				continue;
			}
			vector<int> initJ = t->getInitTwoJ();
			for(auto j: initJ)
				cout<<j<<" ";

			cout<<endl;

			for(auto j: t->getAddTwoJ())
				cout<<j<<" ";

			cout<<endl<<"------------"<<endl;
		}

		cout<<endl;
	}

	cout<<"DONE!"<<endl;
}


//--------------------------------------------------------------------------------
void test_read_zeeman()
{
	cout<<"################ read Zeeman ################"<<endl;


	GlobalConfig cfg;

	ZeemanOpParser hprs("Zeeman");
	Parser p("standart");

	InputParser prs;

	vector<string> tokens;

	try
	{
		//--------------------------------------------
		tokens= prs.ReadScriptFile("fuu");


		//--------------------------------------------
		cout<<"read spins"<<endl;
		if( (p.readArray<int>(cfg.spins,"Spins",tokens)).type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cfg.numberOfSpins = cfg.spins.size();

		//--------------------------------------------
		cout<<"read heis operators"<<endl;

		cout<<"op 1"<<endl;
		RecordInfo rinf = hprs.Parse(&cfg,tokens,tokens.begin());
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cout<<"op 2"<<endl;
		rinf = hprs.Parse(&cfg,tokens,rinf.out_end);
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

 	}
	catch(exception &ex)
	{
		cout<< ex.what()<<endl;
	}

	if( cfg.parameters.size() == 2) cout<<"parameters size "<<cfg.parameters.size()<<", PASSED"<<endl;
	else cout<<"parameters size "<< cfg.parameters.size() <<" , FAIL"<<endl;

	if( cfg.operators.size() == 4) cout<<"operators size "<<cfg.operators.size()<<", PASSED"<<endl;
	else cout<<"operators size "<< cfg.operators.size() <<" , FAIL"<<endl;

	for(auto op: cfg.operators)
	{
		cout<<"### Operator: " << op->GetName() <<endl;
		vector<Tensor*> tens = op->GetAllTensors();

		for(auto t: tens)
		{
				if(t==nullptr)
				{
					cout<<"tensor is null, it could be"<<endl;
					continue;
				}
				vector<int> initJ = t->getInitTwoJ();
				for(auto j: initJ)
					cout<<j<<" ";

				cout<<endl;

				for(auto j: t->getAddTwoJ())
					cout<<j<<" ";

				cout<<endl<<"------------"<<endl;
			}

			cout<<endl;
		}

		cout<<"DONE!"<<endl;
}


//--------------------------------------------------------------------------------
void test_read_ham()
{
	cout<<"################# read Hamiltonian ################"<<endl;


	GlobalConfig cfg;

	HamiltonianParser hprs("Hamiltonian");
	hprs.AddOpParser(new HeisOpParser("Heis"));
	hprs.AddOpParser(new AnisOpParser("Anis"));
	hprs.AddOpParser(new ZeemanOpParser("Zeeman"));

	Parser p("standart");

	InputParser prs;

	vector<string> tokens;

	try
	{
		//--------------------------------------------
		tokens= prs.ReadScriptFile("fuu");


		//--------------------------------------------
		cout<<"read spins"<<endl;
		if( (p.readArray<int>(cfg.spins,"Spins",tokens)).type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;

		cfg.numberOfSpins = cfg.spins.size();

		//--------------------------------------------
		cout<<"read hamiltonian"<<endl;

		RecordInfo rinf = hprs.Parse(&cfg,tokens,tokens.begin());
		if( rinf.type == RecordType::NotFound ) cout<<"not found"<<endl;
		else cout<<"OK"<<endl;		
 	}
	catch(exception &ex)
	{
		cout<< ex.what()<<endl;
	}

	if( cfg.parameters.size() == 2) cout<<"parameters size "<<cfg.parameters.size()<<", PASSED"<<endl;
	else cout<<"parameters size "<< cfg.parameters.size() <<" , FAIL"<<endl;

	if( cfg.operators.size() == 14) cout<<"operators size "<<cfg.operators.size()<<", PASSED"<<endl;
	else cout<<"operators size "<< cfg.operators.size() <<" , FAIL"<<endl;

	cout<<"------- Parameters ---------"<<endl;
	for(auto p: cfg.parameters)
	{
		cout<<p->GetName()<<endl;
	}
	
	for(auto op: cfg.operators)
	{
		cout<<"### Operator: " << op->GetName() <<endl;
		vector<Tensor*> tens = op->GetAllTensors();

		for(auto t: tens)
		{
				if(t==nullptr)
				{
					cout<<"tensor is null, it could be"<<endl;
					continue;
				}
				vector<int> initJ = t->getInitTwoJ();
				for(auto j: initJ)
					cout<<j<<" ";

				cout<<endl;

				for(auto j: t->getAddTwoJ())
					cout<<j<<" ";

				cout<<endl<<"------------"<<endl;
			}

			cout<<endl;
		}

		cout<<"DONE!"<<endl;
}



//--------------------------------------------------------------------------------
void test_read_input()
{
	cout<<"################# read input file ################"<<endl;


	GlobalConfig cfg=
			{
					{},
					0,
					{},
					{},
					false,
					false,
					"",
					{},
					{
							0,
							0,
							0,
							0,
							{},
							"",
							0,
							false,
							"",
							false
					},
					{
							true,
							false,
							true,
							true,
							1,
							0,
							{},
							{},
							"",
							"",
							"",
							false
					}
			};


	InputParser prs;

	vector<string> tokens;

	try
	{
		//--------------------------------------------
		prs.LoadConfigFromFile(&cfg,"fuu");

 	}
	catch(exception &ex)
	{
		cout<< ex.what()<<endl;
		std::exit(1);
	}

	cout<<"isDynamicCalc: "<<cfg.isDynamicCalc<<endl;
	cout<<"isStaticCalc: "<<cfg.isStaticCalc<<endl;

	if( cfg.parameters.size() == 4) cout<<"parameters size "<<cfg.parameters.size()<<", PASSED"<<endl;
	else cout<<"parameters size "<< cfg.parameters.size() <<" , FAIL"<<endl;

	if( cfg.operators.size() == 14) cout<<"operators size "<<cfg.operators.size()<<", PASSED"<<endl;
	else cout<<"operators size "<< cfg.operators.size() <<" , FAIL"<<endl;

	cout<<"------- Parameters ---------"<<endl;
	for(auto p: cfg.parameters)
	{
		cout<<p->GetName()<<"; size "<< p->GetParData().size()<<endl;
	}

	for(auto op: cfg.operators)
	{
		cout<<"### Operator: " << op->GetName() <<endl;
		vector<Tensor*> tens = op->GetAllTensors();
	}

	cout<<"------- Observables Parameters ---------"<<endl;
	for(auto p: cfg.observables)
	{
		cout<<p->GetName()<<"; amount of tensors: "<< p->GetTensors().size() << endl;
	}

	cout<<"------- Variable parameter data ---------"<<endl;
	for(auto vp: cfg.parameters)
	{
		cout<<vp->GetName()<<": ";

		for(auto val: vp->GetParData())
		{
			cout<<val<<" ";
		}
		cout<<endl;
	}

	cout<<"------- density matrices indices ---------"<<endl;

	cout<<"single ";
	for(auto i: cfg.singleDMInds)
	{
		cout<<i<<" ";
	}
	cout<<endl;

	cout<<"double ";
	for(auto i: cfg.doubleDMInds)
	{
		cout<<i.first<<" "<<i.second<<" | ";
	}
	cout<<endl;

	cout<<"DONE!"<<endl;
}




//--------------------------------------------------------------------------------
int main() {

	//cout<<"store init"<<endl;
//	StoreLocator<Tensor>::Initialize();
//	StoreLocator<QOperator>::Initialize();
//	StoreLocator<VarParameter>::Initialize();

//	cout<<"create store"<<endl;
	Store<Tensor> *tStore=new Store<Tensor>("Tensor");
	Store<QOperator> *opStore=new Store<QOperator>("Operator");
	Store<VarParameter> *parStore=new Store<VarParameter>("Parameter");

//	cout<<"Set Store"<<endl;
	StoreLocator<Tensor>::SetStore(tStore);
	StoreLocator<QOperator>::SetStore(opStore);
	StoreLocator<VarParameter>::SetStore(parStore);

	test_read_toks();
	test_parse_val();
	test_parse_array();
	test_parse_bool();
	test_read_block();
	test_read_heis();
	test_read_anis();
	test_read_zeeman();
	test_read_ham();

	test_read_input();



	cout<<" Dispose Tensor Store"<<endl;
	delete tStore;

	cout<<" Dispose Operator Store"<<endl;
	delete opStore;

	cout<<" Dispose Parameter Store"<<endl;
	delete parStore;


	return 0;
}
